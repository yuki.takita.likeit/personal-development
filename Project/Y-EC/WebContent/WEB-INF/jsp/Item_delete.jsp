<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--背景-->
<link rel="stylesheet" href="css/background.css">
<!--javaScriptの読み込み-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<title>item_delete</title>
</head>
<body style="padding-bottom: 50px;">
	<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-light bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<li class="nav-item"><a class="nav-link"
							href="Delivary_company_login">Delivary_company_login</a></li>
						<li class="nav-item"><a class="nav-link" href="Seller_login">Seller_login</a>
						</li>
						<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
						</li>
					</ul>
				</div>
			</nav>
			<a class="navbar-brand" href="Index">EC</a>
			<nav class="navbar navbar-light bg-light">
				<a class="navbar-brand"></a>

				<form class="form-inline" action="Index" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>

			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
	<!--/page_header-->
	<!--page_body-->
	<div class="container mt-5">
		<label class="blockquote text-center">商品削除</label>
	</div>
	<div class="container">
		<form action="item_delete" method="post">
			<div class="row mt-5">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="user_login_id" class="col-md-10 col-form-label mt-2">商品ID ${itemData.item_id}</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="user_password" class="col-md-10 col-form-label mt-2">${itemData.item_name}　を削除してもよろしいですか</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-2"></div>
				<div class="col-md-4">
					<input type="hidden" value="${itemData.item_id}" name="item_id">
					<button type="submit" class="btn btn-secondary mt-2">yes</button>
					</div>
				<div class="col-md-4">
					<a href="Item_list">
					<button type="button" class="btn btn-secondary mt-2">no</button>
					</a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</form>
	</div>
	<!--/page_body-->
	<!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white"> .inc </footer>
	<!--/page_footer-->
</body>
</html>