<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--背景-->
<link rel="stylesheet" href="css/background.css">
<!--javaScriptの読み込み-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<title>item_index_detail</title>
</head>
<body style="padding-bottom: 50px;">
	<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-expand-md bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						</c:if>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
							</li>
						</c:if>
					</ul>
				</div>
			</nav>

			<nav class="navbar-nav ml-auto navbar-light bg-light">

				<a class="navbar-brand"></a>
				<form class="form-inline" action="Index_search" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>

			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
	<!--/page_header-->
	<!--page_buy_histry-->
	<div class="conainer">
		<div class="row mt-5">
			<div class="col-md-2"></div>
			<div class="col-md-8 text-center">
				<h3>カート商品詳細</h3>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	        <div class="row">
        	<div class="col-md-2">
        	</div>
        	<div class="col-md-8 text-center">
				<c:if test="${errMsg != null}">
	        			<div class="alert alert-danger" role="alert">
		        			${errMsg}
		        		</div>
		       	</c:if>
        	</div>
        	<div class="col-md-2">
        	</div>
        </div>
	<div class="container border-bottom mt-5">
		<div class="row align-items-center">
			<div class="col-md-4 text-center">
				<div class="row">
					<div class="col-md-6"><strong>id</strong></div>
					<div class="col-md-6">${userData.user_id}</div>
				</div>
			</div>
			<div class="col-md-4 text-center">
				<div class="row">
					<div class="col-md-6"><strong>ユーザー名</strong></div>
					<div class="col-md-6">${userData.user_name}</div>
				</div>
			</div>
			<div class="col-md-4 text-center">
				<div class="row">
					<div class="col-md-6"><strong>ユーザー住所</strong></div>
					<div class="col-md-6">${userData.user_area_address}</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container mt-5">
		<div class="row border-bottom mb-2 mt-3">
			<div class="col-md-6 text-center"><strong>商品名</strong></div>
			<div class="col-md-6 text-center"><strong>単価</strong></div>
		</div>
		<c:forEach var="Cart" items="${cartList}">
			<div class="row border-bottom mt-2">
				<div class="col-md-6 text-left" style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 1; overflow: hidden;">
					${Cart.item_name}
				</div>
				<div class="col-md-6  text-center">${Cart.numberFormatItemPrice}</div>
			</div>
		</c:forEach>
		<c:if test="${delivaryData!=null}">
			<div class="row border-bottom mt-5">
				<div class="col-md-5 text-center"><strong>配送業者</strong></div>
				<div class="col-md-3 text-center"><strong>配送方法</strong></div>
				<div class="col-md-2 text-center"><strong>配送料</strong></div>
				<div class="col-md-2 text-center"><strong>チャージ</strong></div>
			</div>
			<form action="Cart_buy_detail" method="get">

				<div class="row border-bottom mt-2">
					<div class="col-md-5 text-center">
						${delivaryData.delivary_name}</div>
					<div class="col-md-5 text-center">
						<div class="dropdown card">
							<select class="form-control" id="exampleFormControlSelect1"
								name="method">
								<c:forEach var="DelivaryMethod" items="${delivaryMethodList}">
									<option class="DelivaryMethod"
										value="${DelivaryMethod.delivary_method_id}">
										${DelivaryMethod.delivary_method_name}    料金
										${DelivaryMethod.numberFormatdelivary_method_price}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-md-2  text-center">${buyDetail.numberFormatcharge}</div>
					<input type="hidden" value="${buyDetail.charge}" name="charg">
				</div>
				<input type="hidden" value="${delivaryData.delivary_id}"
					name="delivary_id">
				<div class="row mt-5">
					<div class="col-md-8"></div>
					<div class="col-md-4 text-right">
						<a href="Cart_buy_detail">
							<button type="submit" class="btn btn-secondary">購入内容確認</button>
						</a>
					</div>
				</div>

			</form>
		</c:if>
	</div>

	<!--delivarySerch-->
	<c:if test="${delivaryData==null}">
		<div class="row mt-5 mb-2">
			<div class="col-md-2"></div>
			<div class="col-md-8"><strong>配達業者検索</strong></div>
			<div class="col-md-2"></div>
		</div>
		<div class="row text-center mb-2">
			<div class="col-md-2 text-center"></div>
			<div class="col-md-2 text-center"><strong>配送地域</strong></div>
			<div class="col-md-4 text-center"><strong>配送業者評価範囲</strong></div>
		</div>
		<form action="Delivary_comment_list" method="get">
			<div class="row text-center">
				<div class="col-md-2"></div>
				<div class="col-md-2 text-center">
					<div class="dropdown card text-center">
						<select class="form-control" id="exampleFormControlSelect1"
							name="area">
							<c:forEach var="Area" items="${area}">
								<option class="DelivaryArea"
									value="${Area.area_name_prefectual}">${Area.area_name_prefectual}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="col-md-4 text-center">
					<div class="row">
						<div class="col-md-5 text-center">
							<select class="form-control" id="exampleFormControlSelect1"
								name="min">
								<option class="min" value="0">0</option>
								<option class="min" value="1">1</option>
								<option class="min" value="2">2</option>
								<option class="min" value="3">3</option>
								<option class="min" value="4">4</option>
								<option class="min" value="5">5</option>
							</select>
						</div>
						<div class="col-md-2">~</div>
						<div class="col-md-5 text-center">
							<select class="form-control" id="exampleFormControlSelect1"
								name="max">
								<option class="max" value="1">1</option>
								<option class="max" value="2">2</option>
								<option class="max" value="3">3</option>
								<option class="max" value="4">4</option>
								<option class="max" value="5">5</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-1 text-right">
					<button type="submit" class="btn btn-secondary">検索</button>
				</div>
				<div class="col-md-2"></div>
			</div>
		</form>
	</c:if>
	<!--/delivarySerch-->

	<!--/page_buy_histry-->
	<!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white">
		<div class="row">
			<div class="col-md-6 text-right">.inc</div>
			<div class="col-md-6 text-right"></div>
		</div>
	</footer>
	<!--/page_footer-->
</body>
</html>