<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--背景-->
	<link rel="stylesheet" href="css/background.css">
<!--javaScriptの読み込み-->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<title>delivary_comment_sign_up</title>
</head>
<body style="padding-bottom: 50px;">
	<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-expand-md bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						</c:if>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
							</li>
						</c:if>
					</ul>
				</div>
			</nav>

			<nav class="navbar-nav ml-auto navbar-light bg-light">

				<a class="navbar-brand"></a>
				<form class="form-inline" action="Index_search" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>

			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
    <!--/page_header-->
    <!--page_body-->
<div class="container mt-5">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <div class="col-md-12">
                <h3>口コミ登録</h3>
                </div>
            </div>
         <div class="col-md-2">
        </div>
         <div class="row">
        	<div class="col-md-2">
        	</div>
        	<div class="col-md-8 text-center">
				<c:if test="${errMsg != null}">
	        			<div class="alert alert-danger" role="alert">
		        			${errMsg}
		        		</div>
		       	</c:if>
        	</div>
        	<div class="col-md-2">
        	</div>
        </div>

            <form action="Delivary_comment_sign_up" method="post">
                <div class="form-group mt-5">
                    <div class="row border-bottom">
                        <div class="col-md-6">
                            <label for="exampleFormControlInput1"><strong>ユーザー名</strong></label>
                        </div>
                        <div class="col-md-6">
                            ${userInfo.user_name}
                        </div>
                    </div>
                </div>
                 <div class="form-group">
                    <div class="row border-bottom">
                        <div class="col-md-6">
                            <label for="exampleFormControlInput1"><strong>配送業者</strong></label>
                        </div>
                        <div class="col-md-6">
                            ${buyData.delivary_name}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row border-bottom">
                        <div class="col-md-6">
                            <label for="exampleFormControlSelect1"><strong>配送方法</strong></label>
                        </div>
                        <div class="col-md-6">
                            ${buyData.delivary_method}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="exampleFormControlSelect1"><strong>評価</strong></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control" id="exampleFormControlSelect1" name="select">
                              <option class="select" value="1">1</option>
                              <option class="select" value="2">2</option>
                              <option class="select" value="3">3</option>
                              <option class="select" value="4">4</option>
                              <option class="select" value="5">5</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="exampleFormControlTextarea1"><strong>コメント</strong></label>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="comment"></textarea>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                    <input type="hidden" value="${buyData.buy_id}" name="buy_id">
                    	<a href="Delivary_comment_sign_up" class="btn-floating btn waves-effect waves-light">
                        	<button type="submit" class="btn btn-secondary">登録</button>
                    	</a>
                    </div>
                </div>
                <div class="col-md-2">
                </div>
            </form>

        </div>
    </div>
</div>
    <!--/page_body-->
    <!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white">
		<div class="row">
			<div class="col-md-6 text-right">.inc</div>
			<div class="col-md-6 text-right">
			</div>
		</div>
	</footer>
    <!--/page_footer-->
</body>
</html>