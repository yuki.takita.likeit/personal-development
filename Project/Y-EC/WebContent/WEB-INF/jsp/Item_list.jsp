<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--背景-->
<link rel="stylesheet" href="css/background.css">
<!--javaScriptの読み込み-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<title>item_list</title>
</head>
<body style="padding-bottom: 50px;">

<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-expand-md bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						</c:if>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
							</li>
						</c:if>
					</ul>
				</div>
			</nav>

			<nav class="navbar-nav ml-auto navbar-light bg-light">
				<a class="navbar-brand"></a>
				<form class="form-inline" action="Index_search" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>

			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
<!--/page_header-->

<!--page_body-->

<div class="container-fluid">
	<div class="row">

		<!-- sideberleft -->
			<nav class="col-md-2 d-none d-md-block sidebar">
					<div class="row mt-5" style="position: -webkit-sticky; position: sticky; top: 500px;">
						<div class="col-md-12 card">
							<h4>Earnings</h4>
							<small class="text-muted text-right border-bottom"> 売上金額 ${earnings} </small>
						</div>
					</div>
			</nav>
		<!-- /sideberleft -->

		<!-- main -->
			<div class="col-md-8">
				<!-- title -->
						<div class="row mt-5">
							<div class="col-md-12 text-center">
								<h4>販売者情報</h4>
							</div>
						</div>
				<!-- /title -->

				<!-- sellerInfo -->
						<div class="row mt-3">
							<div class="col-md-12 border-bottom">
								<div class="row">
									<div class="col-md-3">
										<strong>販売者ID</strong> <br>${seller.seller_id}
									</div>
									<div class="col-md-3">
										<strong>販売者名</strong> <br>${seller.seller_name}
									</div>
									<div class="col-md-3">
										<strong>登録日</strong> <br>${seller.formatseller_create_date}
									</div>
									<div class="col-md-3">
										<strong>更新日</strong> <br>${seller.formatseller_update_date}
									</div>
								</div>
							</div>
						</div>
				<!-- /sellerInfo -->

				<!-- selslerInfo_btn -->
						<div class="row mt-3">
							<div class="col-md-12 text-right">
								<a href="Seller_detail">
									<button type="button" class="btn btn-secondary">詳細</button>
								</a>
								<a href="Seller_update">
									<button type="button" class="btn btn-secondary">更新</button>
								</a>
								<a href="Seller_delete">
									<button type="button" class="btn btn-secondary">削除</button>
								</a>
							</div>
						</div>
				<!-- /selslerInfo_btn -->

				<!-- itemTitle -->
						<div class="row mt-5">
							<div class="col-md-12 text-center">
								<h3>登録商品一覧</h3>
							</div>
						</div>
				<!-- /itemTitle -->

				<!-- itemSignUp -->
						<div class="row mt-3">
							<div class="col-md-12 text-right">
								<a href="Item_sign_up">
									<button type="button" class="btn btn-secondary">商品新規登録</button>
								</a>
							</div>
						</div>
				<!-- /itemSignUp -->

				<!-- itemList -->
					<div class="mt-5">
						<c:forEach var="Item" items="${itemList}">
							<div class="row">
								<div class="col-md-3 card img-fluid">
									<img src="img/itemImg/${Item.item_img_name}" class="card-img" style="object-fit:cover;">
								</div>
								<div class="col-md-9 card">
									<div class="card-body">
										<div class="row">
											<div class="col-md-12 border-bottom">
												<h5 class="card-title"
													style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 1; overflow: hidden;">${Item.item_name}</h5>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12"
												style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 5; overflow: hidden;">
												<p class="card-text">${Item.item_detail}</p>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 text-right"></div>
										</div>
									</div>
									<div class="row card-footer">
										<div class="col-md-6 text-left">
											<p class="card-text">
												<small class="text-muted">商品登録日:${Item.formatitem_create_date}</small>
											</p>
										</div>
										<div class="col-md-6 text-right">
											<a href="Item_detail?item_id=${Item.item_id}">
												<button type="button" class="btn btn-secondary">詳細</button>
											</a>
											<a href="Item_update?item_id=${Item.item_id}">
												<button type="button" class="btn btn-secondary">編集</button>
											</a>
											<a href="Item_delete?item_id=${Item.item_id}">
												<button type="button" class="btn btn-secondary">削除</button>
											</a>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				<!-- /itemList -->
			</div>
		<!-- /main -->

		<!--/sideVar-->
			<nav class="col-md-2 d-none d-md-block sidebar">
				<div class="row" style="margin-top:394px;">
					<div class="col-md-12 card">
						<h4>ItemInfo</h4>
						<c:forEach var="Item" items="${itemList}">
							<small class="text-muted"
								style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 1; overflow: hidden;">
								${Item.item_name}
							</small>
							<c:if test="${Item.item_stock == 0 }">
								<small class="text-right" style="color: #F22C5A;">
							</c:if>
							<c:if test="${!(Item.item_stock == 0 )}">
								<small class="text-muted text-right">
							</c:if>
								在庫数　${Item.item_stock} 個
							<c:if test="${!(Item.item_stock == 0 )}">
								</small>
							</c:if>
							<c:if test="${Item.item_stock == 0 }">
								</small>
							</c:if>
							<form action="Item_stock" method="post">
								<div class="text-muted text-right  border-bottom">
								<small class="text-muted text-right  border-bottom">
									<input type="hidden" value="${Item.item_id}" name="item_id">
									<input type="text" placeholder="在庫数修正" name="stock_num">
									<button type="submit" class="btn btn-secondary btn-sm">登録</button>
								</small>
								</div>
							</form>
						</c:forEach>
					</div>
				</div>
			</nav>
		<!--/sideVar-->

	</div>
</div>
<!--/page_body-->

<!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white">
		<div class="row">
			<div class="col-md-6 text-right">.inc</div>
			<div class="col-md-6 text-right"></div>
		</div>
	</footer>
<!--/page_footer-->

</body>
</html>