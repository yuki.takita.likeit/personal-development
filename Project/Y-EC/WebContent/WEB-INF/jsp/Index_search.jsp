<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--背景-->
<link rel="stylesheet" href="css/background.css">
<!--javaScriptの読み込み-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<title>index_search</title>
</head>
<body style="padding-bottom: 80px;">
	<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-expand-md bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						</c:if>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
							</li>
						</c:if>
					</ul>
				</div>
			</nav>

			<nav class="navbar-nav ml-auto navbar-light bg-light">

				<a class="navbar-brand"></a>
				<form class="form-inline" action="Index_search" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>

			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
	<!--/page_header-->
	<!--page_body-->
	<div class="container mt-5">

		<div class="row">
			<c:forEach var="Item" items="${itemList}">
				<div class="col-md-4 mt-3">


					<a  href="Item_index_detail?item_id=${Item.item_id}" style="text-decoration: none ;">
					<div class="card" style="height:600px;">
						<div class="text-center mb-1" style="height:300px">
							<img src="img/itemImg/${Item.item_img_name}" class="img-fluid">
						</div>
						<div class="card-body">
							<div class="border-bottom">
								<h5 class="card-title text-dark"
									style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 1; overflow: hidden;">

									<!-- お気に入りicon -->
									<%-- <c:if test="">
										<i class="material-icons">
											favorite_border
										</i>
									</c:if>

									<c:if test="">
										<i class="material-icons">
											favorite
										</i>
									</c:if> --%>

									${Item.item_name}
									</h5>
							</div>
							<div>
								<h6 class="card-title text-dark">${Item.numberFormatItemPrice}</h6>
							</div>
							<div>
								<p class="card-text text-dark"
									style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 8; overflow: hidden;">${Item.item_detail}</p>
							</div>

						</div>
</div>
						</a>
				</div>
			</c:forEach>
		</div>

	</div>
	<div class="container">
		<div class="row fixed-bottom mb-2 fixed-bottom center-block">
			<div class="col-md-5"></div>
			<div class="col-md-2">
				<nav aria-label="Page navigation example">
					<ul class="pagination">
						<li class="page-item">
							<!-- １ページ戻るボタン  --> <c:choose>
								<c:when test="${pageNum == 1}">
									<li class="disabled mt-2"><a><i class="material-icons">navigate_before</i></a></li>
								</c:when>
								<c:otherwise>
									<li class="waves-effect"><a
										href="Index_search?itemSerch=${itemName}&page_num=${pageNum -1}&categoryId=${categoryId}"
										class="page-link" aria-label="Previous"> <span
											aria-hidden="true">&laquo;</span>
									</a></li>
								</c:otherwise>
							</c:choose> <!-- ページインデックス --> <c:forEach
								begin="${(pageNum - 2) > 0 ? pageNum - 2 : 1}"
								end="${(pageNum + 1) > pageMax ? pageMax : pageNum + 1}"
								step="1" varStatus="status">
								<li class="mt-2 mr-3 ml-3"
									<c:if test="${pageNum == status.index }"> class="active" </c:if>><a
									href="Index_search?itemSerch=${itemSerch}&page_num=${status.index}&categoryId=${categoryId}">${status.index}</a></li>
							</c:forEach> <!-- 1ページ送るボタン --> <c:choose>
								<c:when test="${pageNum == pageMax || pageMax == 0}">
									<li class="disabled"><a><i class="material-icons">navigate_next</i></a></li>
								</c:when>
								<c:otherwise>
									<li class="waves-effect mt2"><a
										href="Index_search?itemSerch=${itemName}&page_num=${pageNum + 1}&categoryId=${categoryId}"
										class="page-link" aria-label="Next"> <span
											aria-hidden="true">&raquo;</span>
									</a></li>
								</c:otherwise>
							</c:choose>
					</ul>
				</nav>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
		<!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white">
		<div class="row">
			<div class="col-md-6 text-right">.inc</div>
			<div class="col-md-6 text-right">
			</div>
		</div>
	</footer>
	<!--/page_footer-->
</body>
</html>