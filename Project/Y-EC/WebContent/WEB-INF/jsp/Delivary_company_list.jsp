<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--背景-->
<link rel="stylesheet" href="WebContent/WEB-INF/css/background.css">
<!--javaScriptの読み込み-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<title>Delivary_company_list</title>
</head>
<body style="padding-bottom: 50px;">
	<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-expand-md bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						</c:if>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
							</li>
						</c:if>
					</ul>
				</div>
			</nav>

			<nav class="navbar-nav ml-auto navbar-light bg-light">

				<a class="navbar-brand"></a>
				<form class="form-inline" action="Index_search" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>

			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
	<!--/page_header-->
	<!--page_body-->
	<div class="container mt-5">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 text-center">
				<h2>配送業者一覧</h2>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>

	<div class="container mt-5">
		<c:forEach var="Delivary" items="${delivary}">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-3  card ">
					<img src="img/delivaryImg/${Delivary.delivary_img_name}"
						class="card-img align-items-center">
				</div>
				<div class="col-md-7 card">
					<div class="card-body">
						<h5 class="card-title border-bottom">${Delivary.delivary_name}</h5>
						<div class="border-bottom">
							<p class="card-text">評価</p>
							<div class="row">
								<c:choose>
									<c:when test="${Delivary.delivary_evalution < 2}">
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-7"></div>
									</c:when>
									<c:when
										test="${Delivary.delivary_evalution < 3 and Delivary.delivary_evalution >= 2}">
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-7"></div>
									</c:when>
									<c:when
										test="${Delivary.delivary_evalution < 4 and Delivary.delivary_evalution >= 3}">
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-1">
											<i class="material-icons"></i>
										</div>
										<div class="col-md-7"></div>
									</c:when>
									<c:when
										test="${Delivary.delivary_evalution < 5 and Delivary.delivary_evalution >= 4}">
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-7"></div>
									</c:when>
									<c:when test="${Delivary.delivary_evalution > 5}">
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-1">
											<i class="material-icons">grade</i>
										</div>
										<div class="col-md-7"></div>
									</c:when>
								</c:choose>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 text-left">
								<p class="card-text">配送エリア:</p>
							</div>
							<div class="col-md-9">
								<p class="card-text">${Delivary.delivary_area_name}</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 text-left">
								<p class="card-text">住所:</p>
							</div>
							<div class="col-md-9">
								<p class="card-text">${Delivary.delivary_address_name}</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-left">
								<p class="card-text">営業文:</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<p class="card-text">${Delivary.delivary_selles_text}</p>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12 text-right">
								<p class="card-text">
									<small class="text-muted">登録日${Delivary.formatdelivary_create_date}
										更新日${Delivary.formatdelivary_update_date}</small>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-6 text-right">
								<a
									href="Delivary_company_detail?delivary_id=${Delivary.delivary_id}">
									<button type="button" class="btn btn-secondary">詳細</button>
								</a>
								<c:if test="${Delivary.delivary_id==delivaryInfo.delivary_id}">
									<a href="Delivary_update?delivary_id=${Delivary.delivary_id}">
										<button type="button" class="btn btn-secondary">更新</button>
									</a>
									<a href="Delivary_delete?delivary_id=${Delivary.delivary_id}">
										<button type="button" class="btn btn-secondary">削除</button>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</c:forEach>
	</div>
	<!--/page_body-->
	<!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white">
		<div class="row">
			<div class="col-md-6 text-right">.inc</div>
			<div class="col-md-6 text-right">
			</div>
		</div>
	</footer>
	<!--/page_footer-->
</body>
</html>