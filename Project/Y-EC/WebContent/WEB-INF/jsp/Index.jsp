<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--背景-->
<link rel="stylesheet" href="css/background.css">
<!--javaScriptの読み込み-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<title>index</title>
</head>
<body style="padding-bottom: 50px;">

<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-expand-md bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						</c:if>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
							</li>
						</c:if>
					</ul>
				</div>
			</nav>

			<nav class="navbar-nav ml-auto navbar-light bg-light">

				<a class="navbar-brand"></a>
				<form class="form-inline" action="Index_search" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>
			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
<!--/page_header-->

<!-- page_body -->

	<!-- container -->
		<div class="container-fluid ">
			<div class="row">
				<!-- sidebarLeft -->
					<nav class="col-md-2 d-none d-md-block sidebar">
						<div class="row mt-5" style="position: -webkit-sticky; position: sticky; top:150px;border:0px;">
						</div>
					</nav>
				<!-- /sidebarLeft -->

				<!-- main -->
					<div class="col-md-8 mt-5">

						<!--  -->
						<div class="row">
							<div class="col-md-12">

								<div id="myCarousel1" class="carousel slide" data-ride="carousel">
									<ol class="carousel-indicators">
										<c:forEach items="${countList}" var="Integer">
											<c:if test="${Integer == 0}">
												<li data-target="#myCarousel1" data-slide-to="${Integer}" class="active"></li>
											</c:if>
											<c:if test="${Integer != 0}">
												<li data-target="#myCarousel1" data-slide-to="${Integer}"></li>
											</c:if>
										</c:forEach>
									</ol>

									<div class="carousel-inner" role="listbox">
										<c:forEach items="${category}" var="category" varStatus="status">
											<c:if test="${status.count == 1}">
												<div class="carousel-item active">
											</c:if>
											<c:if test="${status.count != 1}">
												<div class="carousel-item">
											</c:if>
												<a href="Index_search?categoryId=${category.category_id}">
													<img src="img/categoryImg/${category.category_img_name}" style="width: 100%;height: 400px;object-fit: cover;">
											  		<div class="carousel-caption">
											      		<h3>${category.category_name}</h3>
											      	</div>
											   	</a>
											</div>
										</c:forEach>
									</div>

									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
										</a>
										<a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
										</a>
									</div>
								</div>
							</div>
							<!--  -->

						<!-- itemList -->
								<c:forEach var="Item" items="${itemIndex}" varStatus="status">
									<c:if test="${status.count == 1}">
										<div class="row mt-5">
											<div class="col-md-12 text-center">
												<div class="alert alert-dark text-white" role="alert">
													<strong>${Item.item_category_name}</strong>
												</div>
											</div>
										</div>
									</c:if>

									<c:if test="${status.count % 3 == 1 and !(status.count == 1)}">
										<div class="row mt-5">
											<div class="col-md-12 text-center">
												<div class="alert alert-dark text-white" role="alert">
													<strong>${Item.item_category_name}</strong>
												</div>
											</div>
										</div>
									</c:if>

									<c:if test="${status.count % 3 == 1 and !(status.count == 1)}">
										<div class="row">
									</c:if>

									<c:if test="${status.count == 1}">
										<div class="row">
									</c:if>

											<div class="col-md-4 mt-3">
												<a href="Item_index_detail?item_id=${Item.item_id}"
													style="text-decoration: none;">
													<div class="card" style="height:650px;">
														<div class="text-center mb-1" style="height:350px">
															<img src="img/itemImg/${Item.item_img_name}" class="img-fluid">
														</div>
														<div class="card-body">
															<div class="border-bottom">
																<h5 class="card-title text-dark"
																	style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 1; overflow: hidden;">

																	<!-- お気に入りicon -->
																			<!-- 	<a href="Favorite_create?botton=1">
																					<i class="material-icons">
																						favorite_border
																					</i>
																				</a>
																				<a href="Favorite_create?botton=1">
																				<i class="material-icons">
																					favorite
																				</i>
																			</a> -->

																	${Item.item_name}
																	</h5>
															</div>
															<div>
																<h6 class="card-title text-dark">${Item.numberFormatItemPrice}</h6>
															</div>
															<div>
																<p class="card-text text-dark"
																	style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 8; overflow: hidden;">${Item.item_detail}</p>
															</div>
														</div>
													</div>
												</a>
											</div>
									<c:if test="${status.count % 3 == 0 and !(status.count == 1)}">
										</div>
									</c:if>
								</c:forEach>
					<!-- /itemList -->

				<!-- /main -->

				<!-- sidebarRight -->
					<nav class="col-md-2 d-none d-md-block sidebar">
						<div class="row mt-5" style="position: -webkit-sticky; position: sticky; top:150px;border:0px;">
						</div>
					</nav>
				<!-- /sidebarRight -->

			</div>
		</div>
	<!-- /container -->

<!-- page_body -->

<!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white">
		<div class="row">
			<div class="col-md-6 text-right">.inc</div>
			<div class="col-md-6 text-right">
				<a class="text-white" href="Authorized_people_from">authorized
					people from</a>
			</div>
		</div>
	</footer>
<!--/page_footer-->
</body>
</html>