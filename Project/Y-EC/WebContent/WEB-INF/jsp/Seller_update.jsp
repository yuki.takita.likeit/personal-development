<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--背景-->
<link rel="stylesheet" href="css/background.css">
<!--javaScriptの読み込み-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<title>seller_update</title>
</head>
<body style="padding-bottom: 50px;">
	<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-light bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<li class="nav-item"><a class="nav-link"
							href="Delivary_company_login">Delivary_company_login</a></li>
						<li class="nav-item"><a class="nav-link" href="Seller_login">Seller_login</a>
						</li>
						<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
						</li>
					</ul>
				</div>
			</nav>
			<a class="navbar-brand" href="Index">EC</a>
			<nav class="navbar navbar-light bg-light">
				<a class="navbar-brand"></a>

				<form class="form-inline" action="Index" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>

			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
	<!--/page_header-->
	<!--page_body-->
	<div class="container mt-5">
		<div class="col-md-4"></div>
		<div class="col-md-7">
			<label class="blockquote text-center">販売者情報更新</label>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="container">
		<div class="col-md-4"></div>
		<div class="col-md-7 text-center">
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="container">
		<form action="Seller_update" method="post">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="user_login_id" class="col-form-label mt-2">ログインID</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<input type="text" class="form-control" id="login_id"
						placeholder="seller_login_id" name="seller_login_id">
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="user_password" class="col-form-label mt-2">パスワード</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<input type="password" class="form-control" id="password"
						placeholder="seller_password" name="seller_password">
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="password" class="col-form-label mt-2">パスワード(確認)</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<input type="password" class="form-control" id="password_conf"
						placeholder="seller_password" name="seller_password_conf">
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="user_name" class="col-form-label mt-2">販売者名</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<input type="text" class="form-control" id="seller_name"
						placeholder="seller_name" name="seller_name">
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="address" class="col-form-label mt-2">住所</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="seller_address_name">
							<c:forEach var="area" items="${area}">
								<option>${area.area_name_prefectual}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-8"></div>
				<div class="col-sm-2 text-right">
					<button type="submit" class="btn btn-secondary mt-2">更新</button>
				</div>
				<div class="col-md-2"></div>
			</div>
		</form>
	</div>
	<!--/page_body-->
	<!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white"> .inc </footer>
	<!--/page_footer-->
</body>
</html>