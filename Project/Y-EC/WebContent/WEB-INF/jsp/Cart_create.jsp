<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- EL文読み込み -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--bootStratedの読み込み-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--背景-->
<link rel="stylesheet" href="css/background.css">
<!--javaScriptの読み込み-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
<!--google_iconの読み込み-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<title>cart_create</title>
</head>
<body style="padding-bottom: 50px;">

<!--page_header-->
	<div class="sticky-top">
		<nav class="navbar navbar-expand-md bg-light">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav" aria-
					expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item active"><a class="nav-link" href="Index">Home
								<span class="sr-only">(current)</span>
						</a></li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Cart_create">Cart</a>
						</c:if>
						<li class="nav-item"><a class="nav-link" href="User_login">User_login</a>
						</li>
						<c:if test="${userInfo != null}">
							<li class="nav-item"><a class="nav-link" href="Logout">Logout</a>
							</li>
						</c:if>
					</ul>
				</div>
			</nav>

			<nav class="navbar-nav ml-auto navbar-light bg-light">
				<a class="navbar-brand"></a>
				<form class="form-inline" action="Index_search" method="post">
					<div class="dropdown card">
						<select class="form-control" id="exampleFormControlSelect1"
							name="category">
							<c:forEach var="category" items="${category}">
								<option value="${category.category_name}">${category.category_name}</option>
							</c:forEach>
						</select>
					</div>
					<input class="form-control mr-sm-2" type="text"
						placeholder="item_name" aria-label="Search" name="itemSerch">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>

			</nav>
			<div></div>
			<a class="navbar-brand" href="Index"> <img src="img/logo.png"
				width="80" height="80" alt="">
			</a>
		</nav>
	</div>
<!--/page_header-->

<!--page_body-->
<div class="container-fluid mt-5">
	<div class="row">

		<!-- sidebarLeft -->
			<nav class="col-md-2 d-none d-md-block sidebar">
				<div class="row mt-5" style="position: -webkit-sticky; position: sticky; top:150px;border:0px;">
					<div class="col-md-12 card">
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>userInfo</h4>
							</div>
						</div>
						<div class="row ">
							<div class="col-md-1">
							</div>
							<div class="col-md-5 text-right ">
								ID:
							</div>
							<div class="col-md-5 text-center border-bottom">
								${userData.user_id}
							</div>
							<div class="col-md-1">
							</div>
						</div>
						<div class="row">
							<div class="col-md-1">
							</div>
							<div class="col-md-5 text-right">
								名前:
							</div>
							<div class="col-md-5 text-center border-bottom">
								${userData.user_name}
							</div>
							<div class="col-md-1">
							</div>
						</div>
						<div class="row">
							<div class="col-md-1">
							</div>
							<div class="col-md-5 text-right">
								住所:
							</div>
							<div class="col-md-5 text-center border-bottom">
								${userData.user_area_address}
							</div>
							<div class="col-md-1">
							</div>
						</div>
					</div>
				</div>
			</nav>
		<!-- /sidebarLeft -->

		<!-- main -->
			<div class="col-md-8">

				<!-- title -->
					<div class="row">
						<div class="col-md-12 text-center">
							<h2>カート内商品一覧</h2>
						</div>
					</div>
				<!-- /title -->

				<!-- cartList -->
					<c:forEach var="Cart" items="${cartList}">
						<div class="row">
							<div class="col-md-12">
								<div class="card-group border-top">
									<div class="row" style="display: flex;">
										<div class="col-md-3 text-center img-fluid">
											<img src="img/itemImg/${Cart.item_img_name}" class="card-img"  style="height:150px;width:150px; object-fit: cover;">
										</div>
										<div class="col-md-7">
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 text-left">
														<h5 class="card-title">${Cart.item_name}</h5>
														<p class="card-text">${Cart.numberFormatItemPrice}</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 text-right">
														<p class="card-text"></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row card-footer">
									<div class="col-md-4"></div>
									<div class="col-md-5 mt-1 text-right">
										<small class="text-muted">カート追加日時${Cart.formatcart_item_create_date}</small>
									</div>
									<div class="col-md-3 text-right ">
										<a href="Cart_delete?item_id=${Cart.cart_item_id}"
											class="btn btn-secondary btn-sm"> 削除 </a>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
				<!-- cartList -->

			</div>
		<!-- /main -->

		<!--/sidebarRight-->
			<nav class="col-md-2 d-none d-md-block sidebar">
				<div class="row mt-5" style="position: -webkit-sticky; position: sticky; top:150px;border:0px;">
					<div class="col-md-12 card">
						<h4>CartInfo</h4>
						<c:forEach var="Cart" items="${cartList}">
							<small class="text-muted" style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 1; overflow: hidden;">
								${Cart.item_name}
							</small>
							<small class="text-muted text-right border-bottom">
								${Cart.numberFormatItemPrice}
							</small>
						</c:forEach>
						<h6>totalPrice :${totalPrice}</h6>
						<a href="Cart_buy">
							<button class="btn btn-secondary btn-block mb-1">購入</button>
						</a>
					</div>
				</div>
			</nav>
		<!--/sidebarRight-->

	</div>
</div>
<!--/page_body-->

<!--page_footer-->
	<footer class="fixed-bottom bg-secondary text-white">
		<div class="row">
			<div class="col-md-6 text-right">.inc</div>
			<div class="col-md-6 text-right"></div>
		</div>
	</footer>
<!--/page_footer-->
</body>
</html>