package beans;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class DelivaryComment {
	private int comment_id;
	private int comment_delivary_id;
	private int comment_buy_id;
	private int comment_user_id;
	private int comment_evalution;
	private String comment_detail;
	private Date comment_create_date;
	private int comment_method_id;

	private String delivary_name;
	private int delivary_address;
	private int delivary_area_id;
	private String delivary_selles_text;
	private String delivary_img_name;
	private int delivary_evaluation;
	private Date delivary_create_date;
	private Date delivary_update_date;

	private String area_name_prefectual;



	public DelivaryComment(int comment_id, int comment_delivary_id, int comment_buy_id, int comment_user_id,
			int comment_evalution, String comment_detail, Date comment_create_date, int comment_method_id,
			String delivary_name, int delivary_address, int delivary_area_id, String delivary_selles_text,
			String delivary_img_name, int delivary_evaluation, Date delivary_create_date, Date delivary_update_date,
			String area_name_prefectual) {
		super();
		this.comment_id = comment_id;
		this.comment_delivary_id = comment_delivary_id;
		this.comment_buy_id = comment_buy_id;
		this.comment_user_id = comment_user_id;
		this.comment_evalution = comment_evalution;
		this.comment_detail = comment_detail;
		this.comment_create_date = comment_create_date;
		this.comment_method_id = comment_method_id;
		this.delivary_name = delivary_name;
		this.delivary_address = delivary_address;
		this.delivary_area_id = delivary_area_id;
		this.delivary_selles_text = delivary_selles_text;
		this.delivary_img_name = delivary_img_name;
		this.delivary_evaluation = delivary_evaluation;
		this.delivary_create_date = delivary_create_date;
		this.delivary_update_date = delivary_update_date;
		this.area_name_prefectual = area_name_prefectual;
	}
	public DelivaryComment(int comment_id, int comment_delivary_id, int comment_buy_id, int comment_user_id,
			int comment_evalution, String comment_detail, Date comment_create_date, String delivary_name,
			int delivary_address, int delivary_area_id, String delivary_selles_text, String delivary_img_name,
			int delivary_evaluation, Date delivary_create_date, Date delivary_update_date,
			String area_name_prefectual) {
		super();
		this.comment_id = comment_id;
		this.comment_delivary_id = comment_delivary_id;
		this.comment_buy_id = comment_buy_id;
		this.comment_user_id = comment_user_id;
		this.comment_evalution = comment_evalution;
		this.comment_detail = comment_detail;
		this.comment_create_date = comment_create_date;
		this.delivary_name = delivary_name;
		this.delivary_address = delivary_address;
		this.delivary_area_id = delivary_area_id;
		this.delivary_selles_text = delivary_selles_text;
		this.delivary_img_name = delivary_img_name;
		this.delivary_evaluation = delivary_evaluation;
		this.delivary_create_date = delivary_create_date;
		this.delivary_update_date = delivary_update_date;
		this.area_name_prefectual = area_name_prefectual;
	}
	public DelivaryComment() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getComment_id() {
		return comment_id;
	}
	public void setComment_id(int comment_id) {
		this.comment_id = comment_id;
	}
	public int getComment_delivary_id() {
		return comment_delivary_id;
	}
	public void setComment_delivary_id(int comment_delivary_id) {
		this.comment_delivary_id = comment_delivary_id;
	}
	public int getComment_buy_id() {
		return comment_buy_id;
	}
	public void setComment_buy_id(int comment_buy_id) {
		this.comment_buy_id = comment_buy_id;
	}
	public int getComment_user_id() {
		return comment_user_id;
	}
	public void setComment_user_id(int comment_user_id) {
		this.comment_user_id = comment_user_id;
	}

	public String getComment_detail() {
		return comment_detail;
	}
	public void setComment_detail(String comment_detail) {
		this.comment_detail = comment_detail;
	}
	public Date getComment_create_date() {
		return comment_create_date;
	}
	public void setComment_create_date(Date comment_create_date) {
		this.comment_create_date = comment_create_date;
	}
	public String getDelivary_name() {
		return delivary_name;
	}
	public void setDelivary_name(String delivary_name) {
		this.delivary_name = delivary_name;
	}
	public int getDelivary_address() {
		return delivary_address;
	}
	public void setDelivary_address(int delivary_address) {
		this.delivary_address = delivary_address;
	}
	public int getDelivary_area_id() {
		return delivary_area_id;
	}
	public void setDelivary_area_id(int delivary_area_id) {
		this.delivary_area_id = delivary_area_id;
	}
	public String getDelivary_selles_text() {
		return delivary_selles_text;
	}
	public void setDelivary_selles_text(String delivary_selles_text) {
		this.delivary_selles_text = delivary_selles_text;
	}
	public String getDelivary_img_name() {
		return delivary_img_name;
	}
	public void setDelivary_img_name(String delivary_img_name) {
		this.delivary_img_name = delivary_img_name;
	}
	public int getDelivary_evaluation() {
		return delivary_evaluation;
	}
	public void setDelivary_evaluation(int delivary_evaluation) {
		this.delivary_evaluation = delivary_evaluation;
	}
	public Date getDelivary_create_date() {
		return delivary_create_date;
	}
	public void setDelivary_create_date(Date delivary_create_date) {
		this.delivary_create_date = delivary_create_date;
	}
	public Date getDelivary_update_date() {
		return delivary_update_date;
	}
	public void setDelivary_update_date(Date delivary_update_date) {
		this.delivary_update_date = delivary_update_date;
	}
	public String getArea_name_prefectual() {
		return area_name_prefectual;
	}
	public void setArea_name_prefectual(String area_name_prefectual) {
		this.area_name_prefectual = area_name_prefectual;
	}
	public int getComment_method_id() {
		return comment_method_id;
	}
	public void setComment_method_id(int comment_method_id) {
		this.comment_method_id = comment_method_id;
	}
	public int getComment_evalution() {
		return comment_evalution;
	}
	public void setComment_evalution(int comment_evalution) {
		this.comment_evalution = comment_evalution;
	}
	public String getFormatcomment_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(comment_create_date);
	}
	public String getFormatdelivary_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(delivary_create_date);
	}
	public String getFormatdelivary_update_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(delivary_update_date);
	}




}
