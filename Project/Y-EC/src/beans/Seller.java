package beans;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class Seller implements Serializable {
	private int seller_id;
	private String seller_login_id;
	private String seller_password;
	private String seller_name;
	private int seller_address;
	private Date seller_create_date;
	private Date seller_update_date;

	private String area_name;




	public Seller(int seller_id, String seller_login_id, String seller_password, String seller_name, int seller_address,
			Date seller_create_date, Date seller_update_date, String area_name) {
		super();
		this.seller_id = seller_id;
		this.seller_login_id = seller_login_id;
		this.seller_password = seller_password;
		this.seller_name = seller_name;
		this.seller_address = seller_address;
		this.seller_create_date = seller_create_date;
		this.seller_update_date = seller_update_date;
		this.area_name = area_name;
	}

	public Seller() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public Seller(String login_idData, String name) {
		this.seller_login_id = login_idData;
		this.seller_name = name;
	}

	public int getSeller_id() {
		return seller_id;
	}

	public void setSeller_id(int seller_id) {
		this.seller_id = seller_id;
	}

	public String getSeller_login_id() {
		return seller_login_id;
	}

	public void setSeller_login_id(String seller_login_id) {
		this.seller_login_id = seller_login_id;
	}

	public String getSeller_password() {
		return seller_password;
	}

	public void setSeller_password(String seller_password) {
		this.seller_password = seller_password;
	}

	public String getSeller_name() {
		return seller_name;
	}

	public void setSeller_name(String seller_name) {
		this.seller_name = seller_name;
	}

	public int getSeller_address() {
		return seller_address;
	}

	public void setSeller_address(int seller_address) {
		this.seller_address = seller_address;
	}

	public Date getSeller_create_date() {
		return seller_create_date;
	}

	public void setSeller_create_date(Date seller_create_date) {
		this.seller_create_date = seller_create_date;
	}

	public Date getSeller_update_date() {
		return seller_update_date;
	}

	public void setSeller_update_date(Date seller_update_date) {
		this.seller_update_date = seller_update_date;
	}

	public String getArea_name() {
		return area_name;
	}

	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}
	public String getFormatseller_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(seller_create_date);
	}
	public String getFormatseller_update_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(seller_update_date);
	}

}
