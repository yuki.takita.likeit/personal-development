package beans;

import java.sql.Date;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class Favorite {
	private int favorite_id;
	private int favorite_user_id;
	private int favorite_item_id;
	private Date favorite_item_create_date;
	private int favorite_group_id;
	private String favorite_group_name;
	private Date favorite_group_create_date;
	private int favorite_group_item_id;
	private Date favorite_group_item_create_date;

	private int item_id;
	private int seller_id;
	private String item_name;
	private int item_category_id;
	private String item_detail;
	private int item_price;
	private String item_img_name;
	private int item_count;
	private Date item_create_date;
	private int item_stock;

	public Favorite(int favorite_id, int favorite_user_id, int favorite_item_id, Date favorite_item_create_date,
			int favorite_group_id, String favorite_group_name, Date favorite_group_create_date,
			int favorite_group_item_id, Date favorite_group_item_create_date, int item_id, int seller_id,
			String item_name, int item_category_id, String item_detail, int item_price, String item_img_name,
			int item_count, Date item_create_date, int item_stock) {
		super();
		this.favorite_id = favorite_id;
		this.favorite_user_id = favorite_user_id;
		this.favorite_item_id = favorite_item_id;
		this.favorite_item_create_date = favorite_item_create_date;
		this.favorite_group_id = favorite_group_id;
		this.favorite_group_name = favorite_group_name;
		this.favorite_group_create_date = favorite_group_create_date;
		this.favorite_group_item_id = favorite_group_item_id;
		this.favorite_group_item_create_date = favorite_group_item_create_date;
		this.item_id = item_id;
		this.seller_id = seller_id;
		this.item_name = item_name;
		this.item_category_id = item_category_id;
		this.item_detail = item_detail;
		this.item_price = item_price;
		this.item_img_name = item_img_name;
		this.item_count = item_count;
		this.item_create_date = item_create_date;
		this.item_stock = item_stock;
	}
	public Favorite() {
		super();
	}
	public int getFavorite_id() {
		return favorite_id;
	}
	public void setFavorite_id(int favorite_id) {
		this.favorite_id = favorite_id;
	}
	public int getFavorite_user_id() {
		return favorite_user_id;
	}
	public void setFavorite_user_id(int favorite_user_id) {
		this.favorite_user_id = favorite_user_id;
	}
	public int getFavorite_item_id() {
		return favorite_item_id;
	}
	public void setFavorite_item_id(int favorite_item_id) {
		this.favorite_item_id = favorite_item_id;
	}
	public Date getFavorite_item_create_date() {
		return favorite_item_create_date;
	}
	public void setFavorite_item_create_date(Date favorite_item_create_date) {
		this.favorite_item_create_date = favorite_item_create_date;
	}
	public int getFavorite_group_id() {
		return favorite_group_id;
	}
	public void setFavorite_group_id(int favorite_group_id) {
		this.favorite_group_id = favorite_group_id;
	}
	public String getFavorite_group_name() {
		return favorite_group_name;
	}
	public void setFavorite_group_name(String favorite_group_name) {
		this.favorite_group_name = favorite_group_name;
	}
	public Date getFavorite_group_create_date() {
		return favorite_group_create_date;
	}
	public void setFavorite_group_create_date(Date favorite_group_create_date) {
		this.favorite_group_create_date = favorite_group_create_date;
	}
	public int getFavorite_group_item_id() {
		return favorite_group_item_id;
	}
	public void setFavorite_group_item_id(int favorite_group_item_id) {
		this.favorite_group_item_id = favorite_group_item_id;
	}
	public Date getFavorite_group_item_create_date() {
		return favorite_group_item_create_date;
	}
	public void setFavorite_group_item_create_date(Date favorite_group_item_create_date) {
		this.favorite_group_item_create_date = favorite_group_item_create_date;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public int getSeller_id() {
		return seller_id;
	}
	public void setSeller_id(int seller_id) {
		this.seller_id = seller_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public int getItem_category_id() {
		return item_category_id;
	}
	public void setItem_category_id(int item_category_id) {
		this.item_category_id = item_category_id;
	}
	public String getItem_detail() {
		return item_detail;
	}
	public void setItem_detail(String item_detail) {
		this.item_detail = item_detail;
	}
	public int getItem_price() {
		return item_price;
	}
	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}
	public String getItem_img_name() {
		return item_img_name;
	}
	public void setItem_img_name(String item_img_name) {
		this.item_img_name = item_img_name;
	}
	public int getItem_count() {
		return item_count;
	}
	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}
	public Date getItem_create_date() {
		return item_create_date;
	}
	public void setItem_create_date(Date item_create_date) {
		this.item_create_date = item_create_date;
	}
	public int getItem_stock() {
		return item_stock;
	}
	public void setItem_stock(int item_stock) {
		this.item_stock = item_stock;
	}
	public String getNumberFormatItemPrice() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(item_price);
	}
	public String getFormatfavorite_item_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(favorite_item_create_date);
	}
	public String getFormatfavorite_group_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(favorite_group_create_date);
	}
	public String getFormatfavorite_group_item_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(favorite_group_item_create_date);
	}


}
