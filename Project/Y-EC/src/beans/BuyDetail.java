package beans;

import java.text.NumberFormat;

public class BuyDetail {
	public int total_price;
	public int charge;
	public int tax;

	public BuyDetail(int total_price, int charge) {
		super();
		this.total_price = total_price;
		this.charge = charge;
	}


	public BuyDetail(int total_price, int charge, int tax) {
		super();
		this.total_price = total_price;
		this.charge = charge;
		this.tax = tax;
	}


	public BuyDetail() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getTotal_price() {
		return total_price;
	}

	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}

	public int getCharge() {
		return charge;
	}

	public void setCharge(int charge) {
		this.charge = charge;
	}
	public String getNumberFormattotal_price() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(total_price);
	}
	public String getNumberFormatcharge() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(charge);
	}
	public String getNumberFormattax() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(tax);
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

}
