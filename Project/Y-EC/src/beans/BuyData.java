package beans;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyData {
	private int buy_id;
	private String delivary_method;
	private int buy_total_price;
	private Date buy_create_date;
	private String delivary_name;
	private int delivary_method_id;
	private int buy_user_id;
	private int buy_delivary_id;

	private String item_name;
	private int item_price;



	public BuyData(int buy_id, String delivary_method, int buy_total_price, Date buy_create_date, String delivary_name,
			int delivary_method_id, int buy_user_id, int buy_delivary_id, String item_name, int item_price) {
		super();
		this.buy_id = buy_id;
		this.delivary_method = delivary_method;
		this.buy_total_price = buy_total_price;
		this.buy_create_date = buy_create_date;
		this.delivary_name = delivary_name;
		this.delivary_method_id = delivary_method_id;
		this.buy_user_id = buy_user_id;
		this.buy_delivary_id = buy_delivary_id;
		this.item_name = item_name;
		this.item_price = item_price;
	}




	public BuyData(int buy_id, int delivary_method_id, int buy_user_id, int buy_delivary_id) {
		super();
		this.buy_id = buy_id;
		this.delivary_method_id = delivary_method_id;
		this.buy_user_id = buy_user_id;
		this.buy_delivary_id = buy_delivary_id;
	}




	public BuyData(int buy_id, String delivary_method, int buy_total_price, Date buy_create_date, String delivary_name,
			String item_name, int item_price) {
		super();
		this.buy_id = buy_id;
		this.delivary_method = delivary_method;
		this.buy_total_price = buy_total_price;
		this.buy_create_date = buy_create_date;
		this.delivary_name = delivary_name;
		this.item_name = item_name;
		this.item_price = item_price;
	}


	public BuyData() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getBuy_id() {
		return buy_id;
	}

	public void setBuy_id(int buy_id) {
		this.buy_id = buy_id;
	}

	public String getDelivary_method() {
		return delivary_method;
	}

	public void setDelivary_method(String delivary_method) {
		this.delivary_method = delivary_method;
	}

	public int getBuy_total_price() {
		return buy_total_price;
	}

	public void setBuy_total_price(int buy_total_price) {
		this.buy_total_price = buy_total_price;
	}

	public Date getBuy_create_date() {
		return buy_create_date;
	}

	public void setBuy_create_date(Date buy_create_date) {
		this.buy_create_date = buy_create_date;
	}

	public String getDelivary_name() {
		return delivary_name;
	}

	public void setDelivary_name(String delivary_name) {
		this.delivary_name = delivary_name;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public int getItem_price() {
		return item_price;
	}

	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}


	public int getDelivary_method_id() {
		return delivary_method_id;
	}


	public void setDelivary_method_id(int delivary_method_id) {
		this.delivary_method_id = delivary_method_id;
	}


	public int getBuy_user_id() {
		return buy_user_id;
	}


	public void setBuy_user_id(int buy_user_id) {
		this.buy_user_id = buy_user_id;
	}


	public int getBuy_delivary_id() {
		return buy_delivary_id;
	}
	public String getNumberFormatTotalPrice() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(buy_total_price);
	}
	public String getNumberFormatItemPrice() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(item_price);
	}

	public void setBuy_delivary_id(int buy_delivary_id) {
		this.buy_delivary_id = buy_delivary_id;
	}
	public String getFormatbuy_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(buy_create_date);
	}

}
