package beans;

import java.sql.Date;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class Cart {
	private int cart_id;
	private int cart_user_id;
	private int cart_item_id;
	private Date cart_create_date;

	private Date cart_item_create_date;

	private String item_name;
	private int item_price;
	private String item_detail;
	private String item_img_name;
	private int item_id;
	private int item_stock;




	public Cart(int cart_id, int cart_user_id, int cart_item_id, Date cart_create_date, Date cart_item_create_date,
			String item_name, int item_price, String item_detail, String item_img_name, int item_id) {
		super();
		this.cart_id = cart_id;
		this.cart_user_id = cart_user_id;
		this.cart_item_id = cart_item_id;
		this.cart_create_date = cart_create_date;
		this.cart_item_create_date = cart_item_create_date;
		this.item_name = item_name;
		this.item_price = item_price;
		this.item_detail = item_detail;
		this.item_img_name = item_img_name;
		this.item_id = item_id;
	}

	public Cart(int cart_id, int cart_user_id, int cart_item_id, Date cart_create_date, Date cart_item_create_date,
			String item_name, int item_price, String item_detail, String item_img_name, int item_id, int item_stock) {
		super();
		this.cart_id = cart_id;
		this.cart_user_id = cart_user_id;
		this.cart_item_id = cart_item_id;
		this.cart_create_date = cart_create_date;
		this.cart_item_create_date = cart_item_create_date;
		this.item_name = item_name;
		this.item_price = item_price;
		this.item_detail = item_detail;
		this.item_img_name = item_img_name;
		this.item_id = item_id;
		this.item_stock = item_stock;
	}

	public Cart() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getCart_id() {
		return cart_id;
	}
	public void setCart_id(int cart_id) {
		this.cart_id = cart_id;
	}
	public int getCart_user_id() {
		return cart_user_id;
	}
	public void setCart_user_id(int cart_user_id) {
		this.cart_user_id = cart_user_id;
	}
	public int getCart_item_id() {
		return cart_item_id;
	}
	public void setCart_item_id(int cart_item_id) {
		this.cart_item_id = cart_item_id;
	}
	public Date getCart_create_date() {
		return cart_create_date;
	}
	public void setCart_create_date(Date cart_create_date) {
		this.cart_create_date = cart_create_date;
	}
	public Date getCart_item_create_date() {
		return cart_item_create_date;
	}
	public void setCart_item_create_date(Date cart_item_create_date) {
		this.cart_item_create_date = cart_item_create_date;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public int getItem_price() {
		return item_price;
	}
	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}
	public String getItem_detail() {
		return item_detail;
	}
	public void setItem_detail(String item_detail) {
		this.item_detail = item_detail;
	}
	public String getItem_img_name() {
		return item_img_name;
	}
	public void setItem_img_name(String item_img_name) {
		this.item_img_name = item_img_name;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public String getFormatcart_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(cart_create_date);
	}
	public String getFormatcart_item_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(cart_item_create_date);
	}
	public String getNumberFormatItemPrice() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(item_price);
	}

	public int getItem_stock() {
		return item_stock;
	}

	public void setItem_stock(int item_stock) {
		this.item_stock = item_stock;
	}

}
