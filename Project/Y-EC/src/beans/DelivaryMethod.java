package beans;

import java.text.NumberFormat;

public class DelivaryMethod {
	private int delivary_method_id;
	private String delivary_method_name;
	private int delivary_method_price;
	public DelivaryMethod(int delivary_method_id, String delivary_method_name, int delivary_method_price) {
		super();
		this.delivary_method_id = delivary_method_id;
		this.delivary_method_name = delivary_method_name;
		this.delivary_method_price = delivary_method_price;
	}
	public DelivaryMethod() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getDelivary_method_id() {
		return delivary_method_id;
	}
	public void setDelivary_method_id(int delivary_method_id) {
		this.delivary_method_id = delivary_method_id;
	}
	public String getDelivary_method_name() {
		return delivary_method_name;
	}
	public void setDelivary_method_name(String delivary_method_name) {
		this.delivary_method_name = delivary_method_name;
	}
	public int getDelivary_method_price() {
		return delivary_method_price;
	}
	public void setDelivary_method_price(int delivary_method_price) {
		this.delivary_method_price = delivary_method_price;
	}
	public String getNumberFormatdelivary_method_price() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(delivary_method_price);
	}

}
