package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Delivary  implements Serializable{
	private int delivary_id;
	private String delivary_login_id;
	private String delivary_password;
	private String delivary_name;
	private int delivary_address;
	private int delivary_area_id;
	private String delivary_selles_text;
	private int delivary_evalution;
	private int delivary_comment_id;
	private Date delivary_create_date;
	private Date delivary_update_date;
	private String delivary_img_name;

	private String delivary_address_name;
	private String delivary_area_name;



	public Delivary(int delivary_id, String delivary_login_id, String delivary_password, String delivary_name,
			int delivary_address, int delivary_area_id, String delivary_selles_text, int delivary_evalution,
			int delivary_comment_id, Date delivary_create_date, Date delivary_update_date, String delivary_img_name,
			String delivary_address_name, String delivary_area_name) {
		super();
		this.delivary_id = delivary_id;
		this.delivary_login_id = delivary_login_id;
		this.delivary_password = delivary_password;
		this.delivary_name = delivary_name;
		this.delivary_address = delivary_address;
		this.delivary_area_id = delivary_area_id;
		this.delivary_selles_text = delivary_selles_text;
		this.delivary_evalution = delivary_evalution;
		this.delivary_comment_id = delivary_comment_id;
		this.delivary_create_date = delivary_create_date;
		this.delivary_update_date = delivary_update_date;
		this.delivary_img_name = delivary_img_name;
		this.delivary_address_name = delivary_address_name;
		this.delivary_area_name = delivary_area_name;
	}

	public Delivary(String login_idData, String name) {
		this.delivary_login_id =login_idData;
		this.delivary_name = name;
	}

	public Delivary() {
	}

	public int getDelivary_id() {
		return delivary_id;
	}

	public void setDelivary_id(int delivary_id) {
		this.delivary_id = delivary_id;
	}

	public String getDelivary_login_id() {
		return delivary_login_id;
	}

	public void setDelivary_login_id(String delivary_login_id) {
		this.delivary_login_id = delivary_login_id;
	}

	public String getDelivary_password() {
		return delivary_password;
	}

	public void setDelivary_password(String delivary_password) {
		this.delivary_password = delivary_password;
	}

	public String getDelivary_name() {
		return delivary_name;
	}

	public void setDelivary_name(String delivary_name) {
		this.delivary_name = delivary_name;
	}

	public int getDelivary_address() {
		return delivary_address;
	}

	public void setDelivary_address(int delivary_address) {
		this.delivary_address = delivary_address;
	}

	public int getDelivary_area_id() {
		return delivary_area_id;
	}

	public void setDelivary_area_id(int delivary_area_id) {
		this.delivary_area_id = delivary_area_id;
	}

	public String getDelivary_selles_text() {
		return delivary_selles_text;
	}

	public void setDelivary_selles_text(String delivary_selles_text) {
		this.delivary_selles_text = delivary_selles_text;
	}

	public int getDelivary_evalution() {
		return delivary_evalution;
	}

	public void setDelivary_evalution(int delivary_evalution) {
		this.delivary_evalution = delivary_evalution;
	}

	public int getDelivary_comment_id() {
		return delivary_comment_id;
	}

	public void setDelivary_comment_id(int delivary_comment_id) {
		this.delivary_comment_id = delivary_comment_id;
	}

	public Date getDelivary_create_date() {
		return delivary_create_date;
	}

	public void setDelivary_create_date(Date delivary_create_date) {
		this.delivary_create_date = delivary_create_date;
	}

	public Date getDelivary_update_date() {
		return delivary_update_date;
	}

	public void setDelivary_update_date(Date delivary_update_date) {
		this.delivary_update_date = delivary_update_date;
	}

	public String getDelivary_img_name() {
		return delivary_img_name;
	}

	public void setDelivary_img_name(String delivary_img_name) {
		this.delivary_img_name = delivary_img_name;
	}

	public String getDelivary_address_name() {
		return delivary_address_name;
	}

	public void setDelivary_address_name(String delivary_address_name) {
		this.delivary_address_name = delivary_address_name;
	}

	public String getDelivary_area_name() {
		return delivary_area_name;
	}

	public void setDelivary_area_name(String delivary_area_name) {
		this.delivary_area_name = delivary_area_name;
	}
	public String getFormatdelivary_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(delivary_create_date);
	}
	public String getFormatdelivary_update_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(delivary_update_date);
	}

}
