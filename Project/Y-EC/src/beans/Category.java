package beans;

import java.util.Date;

public class Category {
	private int category_id;
	private String category_name;
	private Date category_create_date;
	private String category_img_name;


	public Category(int category_id, String category_name, Date category_create_date, String category_img_name) {
		super();
		this.category_id = category_id;
		this.category_name = category_name;
		this.category_create_date = category_create_date;
		this.category_img_name = category_img_name;
	}

	public Category(int category_id, String category_name,Date category_create_date) {
		super();
		this.category_id = category_id;
		this.category_name = category_name;
		this.category_create_date = category_create_date;
	}

	public Category() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public Category(int categoryId) {
		this.category_id = categoryId;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public Date getCategory_create_date() {
		return category_create_date;
	}

	public void setCategory_create_date(Date category_create_date) {
		this.category_create_date = category_create_date;
	}

	public String getCategory_img_name() {
		return category_img_name;
	}

	public void setCategory_img_name(String category_img_name) {
		this.category_img_name = category_img_name;
	}

}
