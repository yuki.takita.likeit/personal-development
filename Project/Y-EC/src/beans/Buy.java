package beans;

import java.util.Date;

public class Buy {
	private int buy_id;
	private int buy_user_id;
	private String delivary_method;
	private int delivary_area_id;
	private int buy_total_price;
	private int delivary_id;
	private Date buy_create_date;
	public Buy(int buy_id, int buy_user_id, String delivary_method, int delivary_area_id, int buy_total_price,
			int delivary_id, Date buy_create_date) {
		super();
		this.buy_id = buy_id;
		this.buy_user_id = buy_user_id;
		this.delivary_method = delivary_method;
		this.delivary_area_id = delivary_area_id;
		this.buy_total_price = buy_total_price;
		this.delivary_id = delivary_id;
		this.buy_create_date = buy_create_date;
	}
	public Buy() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getBuy_id() {
		return buy_id;
	}
	public void setBuy_id(int buy_id) {
		this.buy_id = buy_id;
	}
	public int getBuy_user_id() {
		return buy_user_id;
	}
	public void setBuy_user_id(int buy_user_id) {
		this.buy_user_id = buy_user_id;
	}
	public String getDelivary_method() {
		return delivary_method;
	}
	public void setDelivary_method(String delivary_method) {
		this.delivary_method = delivary_method;
	}
	public int getDelivary_area_id() {
		return delivary_area_id;
	}
	public void setDelivary_area_id(int delivary_area_id) {
		this.delivary_area_id = delivary_area_id;
	}
	public int getBuy_total_price() {
		return buy_total_price;
	}
	public void setBuy_total_price(int buy_total_price) {
		this.buy_total_price = buy_total_price;
	}
	public Date getBuy_create_date() {
		return buy_create_date;
	}
	public void setBuy_create_date(Date buy_create_date) {
		this.buy_create_date = buy_create_date;
	}
	public int getDelivary_id() {
		return delivary_id;
	}
	public void setDelivary_id(int delivary_id) {
		this.delivary_id = delivary_id;
	}

}
