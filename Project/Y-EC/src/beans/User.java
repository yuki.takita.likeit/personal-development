package beans;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class User implements Serializable{
	private int user_id;
	private String user_login_id;
	private String user_password;
	private String user_name;
	private int user_address;
	private Date user_birth_date;
	private Date user_create_date;
	private Date user_update_date;

	private String user_area_address;


	public User(int user_id, String user_login_id, String user_password,  String user_name,
			int user_address, Date user_birth_date, Date user_create_date, Date user_update_date,
			String user_area_address) {
		super();
		this.user_id = user_id;
		this.user_login_id = user_login_id;
		this.user_password = user_password;
		this.user_name = user_name;
		this.user_address = user_address;
		this.user_birth_date = user_birth_date;
		this.user_create_date = user_create_date;
		this.user_update_date = user_update_date;
		this.user_area_address = user_area_address;
	}

	public User(String login_id, String name) {
		this.user_login_id = login_id;
		this.user_name = name;
	}

	public User(String login_idData, String name, int address) {
		this.user_login_id = login_idData;
		this.user_name = name;
		this.user_address = address;
	}
	//UserData用
	public User(int user_id2, String user_name2, String user_area_address) {
		this.user_id = user_id2;
		this.user_name = user_name2;
		this.user_area_address = user_area_address;
	}


	public User(int user_id, String user_login_id, String user_name, Date user_birth_date,
			Date user_create_date, Date user_update_date, String user_area_address) {
		super();
		this.user_id = user_id;
		this.user_login_id = user_login_id;
		this.user_name = user_name;
		this.user_birth_date = user_birth_date;
		this.user_create_date = user_create_date;
		this.user_update_date = user_update_date;
		this.user_area_address = user_area_address;
	}


	public User() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_login_id() {
		return user_login_id;
	}

	public void setUser_login_id(String user_login_id) {
		this.user_login_id = user_login_id;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public int getUser_address() {
		return user_address;
	}

	public void setUser_address(int user_address) {
		this.user_address = user_address;
	}

	public Date getUser_birth_date() {
		return user_birth_date;
	}

	public void setUser_birth_date(Date user_birth_date) {
		this.user_birth_date = user_birth_date;
	}

	public Date getUser_create_date() {
		return user_create_date;
	}

	public void setUser_create_date(Date user_create_date) {
		this.user_create_date = user_create_date;
	}

	public Date getUser_update_date() {
		return user_update_date;
	}

	public void setUser_update_date(Date user_update_date) {
		this.user_update_date = user_update_date;
	}

	public String getUser_area_address() {
		return user_area_address;
	}

	public void setUser_area_address(String user_area_address) {
		this.user_area_address = user_area_address;
	}
	public String getFormatBirthDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(user_birth_date);
	}
	public String getFormatCreateDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(user_create_date);
	}
	public String getFormatUpdateDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(user_update_date);
	}
}

