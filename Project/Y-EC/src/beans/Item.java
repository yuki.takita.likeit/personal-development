package beans;

import java.io.Serializable;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class Item implements Serializable{
	private int item_id;
	private int seller_id;
	private String item_name;
	private int item_category_id;
	private String item_detail;
	private int item_price;
	private String item_img_name;
	private int item_count;
	private Date item_create_date;
	private int item_stock;

	private String item_seller_name;
	private String item_seller_address_name;
	private String item_category_name;

	private int favorite_item;


	public Item(int item_id, int seller_id, String item_name, int item_category_id, String item_detail, int item_price,
			String item_img_name, int item_count, Date item_create_date, int item_stock) {
		super();
		this.item_id = item_id;
		this.seller_id = seller_id;
		this.item_name = item_name;
		this.item_category_id = item_category_id;
		this.item_detail = item_detail;
		this.item_price = item_price;
		this.item_img_name = item_img_name;
		this.item_count = item_count;
		this.item_create_date = item_create_date;
		this.item_stock = item_stock;
	}


	public Item(int item_id, int seller_id, String item_name, int item_category_id, String item_detail, int item_price,
			String item_img_name, int item_count, Date item_create_date, int item_stock, String item_seller_name,
			String item_seller_address_name, String item_category_name) {
		super();
		this.item_id = item_id;
		this.seller_id = seller_id;
		this.item_name = item_name;
		this.item_category_id = item_category_id;
		this.item_detail = item_detail;
		this.item_price = item_price;
		this.item_img_name = item_img_name;
		this.item_count = item_count;
		this.item_create_date = item_create_date;
		this.item_stock = item_stock;
		this.item_seller_name = item_seller_name;
		this.item_seller_address_name = item_seller_address_name;
		this.item_category_name = item_category_name;
	}


	public Item(int item_id, int seller_id, String item_name, int item_category_id, String item_detail, int item_price,
			String item_img_name, int item_count, Date item_create_date, int item_stock, String item_seller_name,
			String item_seller_address_name, String item_category_name, int favorite_item) {
		super();
		this.item_id = item_id;
		this.seller_id = seller_id;
		this.item_name = item_name;
		this.item_category_id = item_category_id;
		this.item_detail = item_detail;
		this.item_price = item_price;
		this.item_img_name = item_img_name;
		this.item_count = item_count;
		this.item_create_date = item_create_date;
		this.item_stock = item_stock;
		this.item_seller_name = item_seller_name;
		this.item_seller_address_name = item_seller_address_name;
		this.item_category_name = item_category_name;
		this.favorite_item = favorite_item;
	}


	public Item(int itemId, String itemName) {
		this.item_id = itemId;
		this.item_img_name = itemName;
	}
	public Item() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public int getSeller_id() {
		return seller_id;
	}
	public void setSeller_id(int seller_id) {
		this.seller_id = seller_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public int getItem_category_id() {
		return item_category_id;
	}
	public void setItem_category_id(int item_category_id) {
		this.item_category_id = item_category_id;
	}
	public String getItem_detail() {
		return item_detail;
	}
	public void setItem_detail(String item_detail) {
		this.item_detail = item_detail;
	}
	public int getItem_price() {
		return item_price;
	}
	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}
	public String getItem_img_name() {
		return item_img_name;
	}
	public void setItem_img_name(String item_img_name) {
		this.item_img_name = item_img_name;
	}
	public int getItem_count() {
		return item_count;
	}
	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}
	public Date getItem_create_date() {
		return item_create_date;
	}
	public void setItem_create_date(Date item_create_date) {
		this.item_create_date = item_create_date;
	}
	public int getItem_stock() {
		return item_stock;
	}
	public void setItem_stock(int item_stock) {
		this.item_stock = item_stock;
	}


	public String getItem_seller_name() {
		return item_seller_name;
	}


	public void setItem_seller_name(String item_seller_name) {
		this.item_seller_name = item_seller_name;
	}


	public String getItem_seller_address_name() {
		return item_seller_address_name;
	}


	public void setItem_seller_address_name(String item_seller_address_name) {
		this.item_seller_address_name = item_seller_address_name;
	}


	public String getItem_category_name() {
		return item_category_name;
	}


	public void setItem_category_name(String item_category_name) {
		this.item_category_name = item_category_name;
	}
	public String getFormatitem_create_date() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(item_create_date);
	}
	public String getNumberFormatItemPrice() {
		NumberFormat nfCur = NumberFormat.getCurrencyInstance();
		return nfCur.format(item_price);
	}


	public int getFavorite_item() {
		return favorite_item;
	}


	public void setFavorite_item(int favorite_item) {
		this.favorite_item = favorite_item;
	}
}
