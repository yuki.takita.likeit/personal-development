package beans;

import java.io.Serializable;

public class Area implements Serializable{
	private int area_id;
	private String area_name_prefectual;

	public Area(int area_id, String area_name_prefectual) {
		super();
		this.area_id = area_id;
		this.area_name_prefectual = area_name_prefectual;
	}

	public Area() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public Area(int areaId) {
		this.area_id = areaId;
	}

	public int getArea_id() {
		return area_id;
	}

	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}

	public String getArea_name_prefectual() {
		return area_name_prefectual;
	}

	public void setArea_name_prefectual(String area_name_prefectual) {
		this.area_name_prefectual = area_name_prefectual;
	}


}
