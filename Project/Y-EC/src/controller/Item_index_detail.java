package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Item;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Item_index_detail
 */
@WebServlet("/Item_index_detail")
public class Item_index_detail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Item_index_detail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索カテゴリーの取得
			HttpSession session = request.getSession();
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 商品データの取得
			Item item = new Item();
			item.setItem_id(Integer.parseInt(request.getParameter("item_id")));
			ItemDao itemDao = new ItemDao();
			Item itemIndex = itemDao.findByItemDataDetail(item.getItem_id());
			session.setAttribute("itemIndex", itemIndex);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Item_index_detail.jsp");
			dispatcher.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
