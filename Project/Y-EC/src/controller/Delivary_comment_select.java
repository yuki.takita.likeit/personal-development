package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DelivaryComment;
import dao.DelivaryCommentDao;

/**
 * Servlet implementation class Delivary_comment_select
 */
@WebServlet("/Delivary_comment_select")
public class Delivary_comment_select extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_comment_select() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

			String delivary_id = request.getParameter("delivary_id");
			int deluivaryId = Integer.parseInt(delivary_id);

		//TODO delivary_idで口コミ検索
			DelivaryCommentDao delivaryCommentDao =new DelivaryCommentDao();
			ArrayList<DelivaryComment> commentList = delivaryCommentDao.findComment(deluivaryId);
			request.setAttribute("commentList", commentList);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Cart_buy.jsp");
			dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
