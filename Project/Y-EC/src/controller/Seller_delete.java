package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Seller;
import dao.AreaDao;
import dao.CategoryDao;
import dao.SellerDao;

/**
 * Servlet implementation class Seller_delete
 */
@WebServlet("/Seller_delete")
public class Seller_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Seller_delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("sellerInfo")==null) {
				response.sendRedirect("Index");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			session.setAttribute("area", area.findAll());

		//TODO 販売者データの取得
			SellerDao sellerDao = new SellerDao();
			Seller sellerInfo =  (Seller) session.getAttribute("sellerInfo");
			Seller sellerDetail = sellerDao.findBySellerDetail(sellerInfo.getSeller_login_id());
			request.setAttribute("sellerDetail", sellerDetail);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Seller_delete.jsp");
			dispatcher.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("utf-8");

		String seller_id = request.getParameter("seller_id");
		int sellerId = Integer.parseInt(seller_id);

		SellerDao sellerDao = new SellerDao();
		sellerDao.sellerDelete(sellerId);

		response.sendRedirect("Index");
	}

}
