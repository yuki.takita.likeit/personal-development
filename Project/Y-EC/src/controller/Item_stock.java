package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AreaDao;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Item_stock
 */
@WebServlet("/Item_stock")
public class Item_stock extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Item_stock() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	//TODO ログインセッションがある場合indexServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("sellerInfo")==null) {
			response.sendRedirect("Index");
			return;
		}
	//TODO 検索カテゴリーの取得
		CategoryDao category = new CategoryDao();
		request.setAttribute("category", category.findAll());

	//TODO 地域データの取得
		AreaDao area = new AreaDao();
		request.setAttribute("area", area.findAll());

	//TODO 在庫数の登録
		String item_id = request.getParameter("item_id");
		Integer itemId = Integer.parseInt(item_id);
		String stock_num = request.getParameter("stock_num");
		Integer stockNum = Integer.parseInt(stock_num);
		ItemDao itemDao = new ItemDao();
		itemDao.itemUpdateStock(itemId, stockNum);
		response.sendRedirect("Item_list");
		return;

	}

}
