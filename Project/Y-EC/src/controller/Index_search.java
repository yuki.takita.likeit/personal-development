package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.Item;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Index_search
 */
@WebServlet("/Index_search")
public class Index_search extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//1ページに表示する商品数
	final static int PAGE_MAX_ITEM_COUNT = 3;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index_search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索結果がある場合は削除する
		HttpSession session = request.getSession();
		if(session.getAttribute("itemSerch")!=null) {
			session.removeAttribute("itemSerch");
		}
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

			try {
				//TODO ページ番号の取得
					int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

				//TODO リクエストパラメータの取得
					String itemSerch = request.getParameter("itemSerch");
					CategoryDao categoryDao = new CategoryDao();
					if(request.getParameter("category") != null) {
						String categoryName = request.getParameter("category");
						Category category_id = categoryDao.findByCategoryId(categoryName);
						int categoryId = category_id.getCategory_id();
						request.setAttribute("categoryId",categoryId);
						request.setAttribute("itemSerch",itemSerch);
						ArrayList<Item> searchResultItemList = ItemDao.getItemsByItemName(itemSerch,categoryId, pageNum, PAGE_MAX_ITEM_COUNT);
						request.setAttribute("itemList", searchResultItemList);
					// 検索ワードに対しての総ページ数を取得
						double itemCount = ItemDao.getItemCount(itemSerch,categoryId);
						int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);
					//総アイテム数
						request.setAttribute("itemCount", (int) itemCount);
					// 総ページ数
						request.setAttribute("pageMax", pageMax);
					// 表示ページ
						request.setAttribute("pageNum", pageNum);
					}else{
						String category_id = request.getParameter("categoryId");
						int categoryId = Integer.parseInt(category_id);
						request.setAttribute("categoryId",categoryId);
						ArrayList<Item> searchResultItemList = ItemDao.getItemsByItemName("",categoryId, pageNum, PAGE_MAX_ITEM_COUNT);
						request.setAttribute("itemList", searchResultItemList);
					// 検索ワードに対しての総ページ数を取得
						double itemCount = ItemDao.getItemCount("",categoryId);
						int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);
					//総アイテム数
						request.setAttribute("itemCount", (int) itemCount);
					// 総ページ数
						request.setAttribute("pageMax", pageMax);
					// 表示ページ
						request.setAttribute("pageNum", pageNum);
					}
				//TODO フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Index_search.jsp");
					dispatcher.forward(request, response);

			}catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
				}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索結果がある場合は削除する
		HttpSession session = request.getSession();
		if(session.getAttribute("itemSerch")!=null) {
			session.removeAttribute("itemSerch");
		}
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

			try {
				//TODO ページ番号の取得
					int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

				//TODO リクエストパラメータの取得
					String itemSerch = request.getParameter("itemSerch");
					CategoryDao categoryDao = new CategoryDao();
					if(request.getParameter("category") != null) {
						String categoryName = request.getParameter("category");
						Category category_id = categoryDao.findByCategoryId(categoryName);
						int categoryId = category_id.getCategory_id();
						request.setAttribute("categoryId",categoryId);
						request.setAttribute("itemSerch",itemSerch);
						ArrayList<Item> searchResultItemList = ItemDao.getItemsByItemName(itemSerch,categoryId, pageNum, PAGE_MAX_ITEM_COUNT);
						request.setAttribute("itemList", searchResultItemList);
					// 検索ワードに対しての総ページ数を取得
						double itemCount = ItemDao.getItemCount(itemSerch,categoryId);
						int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);
					//総アイテム数
						request.setAttribute("itemCount", (int) itemCount);
					// 総ページ数
						request.setAttribute("pageMax", pageMax);
					// 表示ページ
						request.setAttribute("pageNum", pageNum);
					}else{
						String category_id = request.getParameter("categoryId");
						int categoryId = Integer.parseInt(category_id);
						request.setAttribute("categoryId",categoryId);
						request.setAttribute("itemSerch",itemSerch);
						ArrayList<Item> searchResultItemList = ItemDao.getItemsByItemName(itemSerch,categoryId, pageNum, PAGE_MAX_ITEM_COUNT);
						request.setAttribute("itemList", searchResultItemList);
					// 検索ワードに対しての総ページ数を取得
						double itemCount = ItemDao.getItemCount(itemSerch,categoryId);
						int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);
					//総アイテム数
						request.setAttribute("itemCount", (int) itemCount);
					// 総ページ数
						request.setAttribute("pageMax", pageMax);
					// 表示ページ
						request.setAttribute("pageNum", pageNum);
					}
				//TODO フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Index_search.jsp");
					dispatcher.forward(request, response);

			}catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
				}

	}
}
