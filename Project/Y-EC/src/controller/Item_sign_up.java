package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.Seller;
import dao.AreaDao;
import dao.CategoryDao;
import dao.ItemDao;
import dao.SellerDao;

/**
 * Servlet implementation class Item_sign_up
 */
@WebServlet("/Item_sign_up")
@MultipartConfig(location="C:\\Users\\takico\\Documents\\Git\\Personal development\\Project\\Y-EC\\WebContent\\img\\itemImg", maxFileSize=1048576)
public class Item_sign_up extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Item_sign_up() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("sellerInfo")==null) {
				response.sendRedirect("Index");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			session.setAttribute("area", area.findAll());

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Item_sign_up.jsp");
			dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO 新規登録レコードの取得
			String itemName = request.getParameter("item_name");
			String itemCategory = request.getParameter("item_category");
			String itemDetail = request.getParameter("item_detail");
			String item_price = request.getParameter("item_price");
			int itemPrice = Integer.parseInt(item_price);
			String item_stock = request.getParameter("item_stock");
			int itemStock = Integer.parseInt(item_stock);
			Part imgFile = request.getPart("item_img");

		//TODO 画像ファイル名を取得
			String imgName = this.getFileName(imgFile);

		//TODO areaIDの取得
			CategoryDao categoryDao = new CategoryDao();
			int categoryId = categoryDao.findByCategoryId(itemCategory).getCategory_id();

		//TODO 販売者ID取得
			HttpSession session = request.getSession();
			Seller seller = (Seller)session.getAttribute("sellerInfo");
			String sellerLoginId = seller.getSeller_login_id();
			SellerDao sellerDao = new SellerDao();
			Seller sellerId = sellerDao.findById(sellerLoginId);


		//TODO DBに新規登録
			ItemDao itemDao = new ItemDao();
			int count = itemDao.itemSignUp(itemName, categoryId, itemDetail, itemPrice, itemStock, imgName, sellerId.getSeller_id());

		//TODO errMsgのセット
			if(count == 0) {
				request.setAttribute("errMsg", "入力データに誤りがあります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Item_sign_up.jsp");
				dispacher.forward(request, response);
				return;
			}else if(count >= 1) {
				imgFile.write(imgName);
				response.sendRedirect("Item_list");
			}
	}
	 private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }
}
