package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Delivary;
import dao.CategoryDao;
import dao.DelivaryDao;

/**
 * Servlet implementation class Delivary_company_detail
 */
@WebServlet("/Delivary_company_detail")
public class Delivary_company_detail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_company_detail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("delivaryInfo")==null) {
				response.sendRedirect("Index");
				return;
			}

		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 配送業者詳細情報を取得
			String delivary_id = request.getParameter("delivary_id");
			int delivaryId = Integer.parseInt(delivary_id);
			DelivaryDao delivaryDao = new DelivaryDao();
			Delivary delivaryData = delivaryDao.DelivaryDetail(delivaryId);
			request.setAttribute("delivaryData", delivaryData);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_company_detail.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
