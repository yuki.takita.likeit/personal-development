package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Area;
import beans.DelivaryComment;
import dao.AreaDao;
import dao.CategoryDao;
import dao.DelivaryCommentDao;

/**
 * Servlet implementation class Delivary_comment_lisy
 */
@WebServlet("/Delivary_comment_list")
public class Delivary_comment_list extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_comment_list() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索カテゴリーの取得
			HttpSession session = request.getSession();
			CategoryDao category = new CategoryDao();
			request.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao areaDao = new AreaDao();
			request.setAttribute("area", areaDao.findAll());


		//TODO 口コミデータの取得
			if(request.getParameter("min")!=null) {
			String delivaryArea = request.getParameter("area");
			String min = request.getParameter("min");
			String max = request.getParameter("max");
			Area area = areaDao.findByAreaId(delivaryArea);
			DelivaryCommentDao delivaryCommentDao = new DelivaryCommentDao();
			ArrayList<DelivaryComment> delivaryCommentList = delivaryCommentDao.findDelivary(min, max, area.getArea_id());
			request.setAttribute("DCL", delivaryCommentList);
			}else {
			//TODO 配送業者データの取得
				DelivaryCommentDao delivaryCommentDao = new DelivaryCommentDao();
				ArrayList<DelivaryComment> delivaryList = delivaryCommentDao.findAll();
				request.setAttribute("delivaryList", delivaryList);
			}
		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_comment_list.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
