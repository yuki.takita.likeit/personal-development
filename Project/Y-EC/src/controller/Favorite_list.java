package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Favorite;
import beans.User;
import dao.CategoryDao;
import dao.FavoriteDao;
import dao.UserDao;

/**
 * Servlet implementation class Favorite_list
 */
@WebServlet("/Favorite_list")
public class Favorite_list extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Favorite_list() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	//TODO ログインセッションがない場合indexServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("Index");
			return;
		}
	//TODO 検索カテゴリーの取得
		CategoryDao category = new CategoryDao();
		request.setAttribute("category", category.findAll());

	//TODO ユーザーデータの取得
		UserDao userDao = new UserDao();
		User user =  (User) session.getAttribute("userInfo");
		User userData = userDao.findByUserData(user.getUser_id());
		request.setAttribute("userData", userData);

	//TODO お気に入りデータの取得
		FavoriteDao favoriteDao = new FavoriteDao();
		ArrayList<Favorite> favoriteList = favoriteDao.findFavorite(userData.getUser_id());
		request.setAttribute("favoriteList",favoriteList);

	//TODO お気に入りグループのデータを取得
		ArrayList<Favorite> favoriteGroupList = favoriteDao.findFavoriteGroup(userData.getUser_id());
		request.setAttribute("facoriteGroupList", favoriteGroupList);

	//TODO フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Favorite_list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("Index");
			return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			request.setAttribute("category", category.findAll());

		//TODO ユーザーデータの取得
			UserDao userDao = new UserDao();
			User user =  (User) session.getAttribute("userInfo");
			User userData = userDao.findByUserData(user.getUser_id());
			request.setAttribute("userData", userData);

		//TODO 選択した商品をお気に入りグループに登録する
			ArrayList<Integer> list = (ArrayList<Integer>) request.getAttribute("list");
			String groupName = (String) request.getAttribute("groupName");
			FavoriteDao favoriteDao = new FavoriteDao();
			Favorite favorite = favoriteDao.findFavoriteId(userData.getUser_id());
			int count = favoriteDao.FavoriteGroupTableSignUp(favorite.getFavorite_id(),groupName);
			Favorite favoriteGroup = favoriteDao.findFavoriteGroupId(groupName);
			if(count >= 1) {
				//groupに商品を登録する。
				for(Integer i : list) {

					int count1 = favoriteDao.FavoriteGroupItemTableSignUp(favoriteGroup.getFavorite_id(),i);

					if(count1 == 0) {
						//TODO エラーメッセージのセット
						request.setAttribute("errMsg", "グループに商品を登録できませんでした");
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Favorite_list.jsp");
						dispatcher.forward(request, response);
						return;
					}
				}
			}else {
				//TODO エラーメッセージのセット
				request.setAttribute("errMsg", "グループを作成できませんでした");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Favorite_list.jsp");
				dispatcher.forward(request, response);
				return;
			}

		//TODO お気に入りデータの取得
			ArrayList<Favorite> favoriteList = favoriteDao.findFavorite(userData.getUser_id());
			request.setAttribute("favoriteList",favoriteList);

		//TODO お気に入りグループのデータを取得
			ArrayList<Favorite> favoriteGroupList = favoriteDao.findFavoriteGroup(userData.getUser_id());
			request.setAttribute("facoriteGroupList", favoriteGroupList);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Favorite_list.jsp");
			dispatcher.forward(request, response);
	}

}
