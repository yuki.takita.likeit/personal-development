package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.Delivary;
import dao.AreaDao;
import dao.CategoryDao;
import dao.DelivaryCommentDao;
import dao.DelivaryDao;

/**
 * Servlet implementation class Delivary_sign_up
 */
@WebServlet("/Delivary_sign_up")
@MultipartConfig(location="C:\\Users\\takico\\Documents\\Git\\Personal development\\Project\\Y-EC\\WebContent\\img\\delivaryImg", maxFileSize=1048576)
public class Delivary_sign_up extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_sign_up() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索カテゴリーの取得
			HttpSession session = request.getSession();
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			session.setAttribute("area", area.findAll());

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_sign_up.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("utf-8");

		//TODO 新規登録レコードの取得
			String login_id = request.getParameter("delivary_login_id");
			String password = request.getParameter("delivary_password");
			String password_conf = request.getParameter("delivary_password_conf");
			String name = request.getParameter("delivary_name");
			String address = request.getParameter("delivary_address_name");
			String delivary_address = request.getParameter("delivary_area_address");
			String sellesText = request.getParameter("delivary_selles_text");
			Part imgFile = request.getPart("delivary_img");
		//TODO 画像ファイル名を取得
			String imgName = this.getFileName(imgFile);

		//TODO areaIDの取得
			AreaDao areaDao = new AreaDao();
			int addressId = areaDao.findByAreaId(address).getArea_id();
			int delivaryAddressId = areaDao.findByAreaId(delivary_address).getArea_id();

			if(!(password.equals(password_conf))) {
				request.setAttribute("errMsg", "パスワードとパスワード(確認)が異なります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Item_sign_up.jsp");
				dispacher.forward(request, response);
				return;
			}

		//TODO DBに新規登録
			DelivaryDao delivaryDao = new DelivaryDao();
			int count = delivaryDao.delivarySignUp(login_id, password, name, addressId,delivaryAddressId,sellesText,imgName);

		//TODO errMsgのセット
			HttpSession session = request.getSession();
			if(count == 0) {
				request.setAttribute("errMsg", "入力データに誤りがあります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_sign_up.jsp");
				dispacher.forward(request, response);
				return;
			}else if(count >= 1) {
				Delivary delivary = delivaryDao.findByInfo(login_id, password);
				//TODO 口コミに初期値を登録
				DelivaryCommentDao delivaryCommentDao = new DelivaryCommentDao();
				delivaryCommentDao.commentSignUp(delivary.getDelivary_id(), 0, 0, 1, "1", "initial value");
				//TODO 画像データの保存
				imgFile.write(imgName);
				session.setAttribute("delivaryInfo", delivary);
				response.sendRedirect("Delivary_company_list");
			}
	}

	 private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }
}
