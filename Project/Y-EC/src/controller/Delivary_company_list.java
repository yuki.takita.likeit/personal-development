package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Delivary;
import dao.AreaDao;
import dao.CategoryDao;
import dao.DelivaryDao;

/**
 * Servlet implementation class Delivary_company_list
 */
@WebServlet("/Delivary_company_list")
public class Delivary_company_list extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_company_list() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索カテゴリーの取得
			HttpSession session = request.getSession();
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			session.setAttribute("area", area.findAll());

		//TODO 配送業者データの取得
			DelivaryDao delivaryDao = new DelivaryDao();
			ArrayList<Delivary> delivaryList = delivaryDao.findAllList();
			session.setAttribute("delivary", delivaryList);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_company_list.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
