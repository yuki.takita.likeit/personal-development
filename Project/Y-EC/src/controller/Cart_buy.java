package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDetail;
import beans.Cart;
import beans.Delivary;
import beans.DelivaryMethod;
import beans.User;
import dao.AreaDao;
import dao.CartDao;
import dao.CategoryDao;
import dao.DelivaryDao;
import dao.DelivaryMethodDao;
import dao.UserDao;

/**
 * Servlet implementation class Cart_buy
 */
@WebServlet("/Cart_buy")
public class Cart_buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart_buy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがある場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("userInfo")==null) {
				response.sendRedirect("Index");
				return;
				}

		//TODO ユーザーデータの取得
			UserDao userDao = new UserDao();
			User user =  (User) session.getAttribute("userInfo");
			User userData = userDao.findByUserData(user.getUser_id());
			session.setAttribute("userData", userData);

		//TODO カート内アイテムデータ取得
			CartDao cartDao = new CartDao();
			Cart cart = new Cart();
			cart = cartDao.CartFindUserId(user.getUser_id());
			ArrayList<Cart> cartList = cartDao.CartItemFindUserId(cart.getCart_id());
			request.setAttribute("cartList", cartList);

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			request.setAttribute("area", area.findAll());

		//TODO
			DelivaryMethodDao delivaryMethodDao = new DelivaryMethodDao();
			ArrayList<DelivaryMethod> delivaryMethodList = delivaryMethodDao.findAll();
			request.setAttribute("delivaryMethodList",delivaryMethodList);

		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			request.setAttribute("category", category.findAll());

		//TODO
			DelivaryDao delivaryDao = new DelivaryDao();
			Object select = request.getParameter("select_delivary_id");
			if(select !=null) {
				int delivaryId = Integer.parseInt((String) select);
				Delivary delivary = delivaryDao.DelivaryDetail(delivaryId);
				request.setAttribute("delivaryData", delivary);
				int evalution = delivary.getDelivary_evalution();
				BuyDetail buyDetail = new BuyDetail();
				if(evalution<=2) {
					buyDetail.setCharge(200);
					request.setAttribute("buyDetail",buyDetail);
				}else if(evalution <=3 && evalution >2) {
					buyDetail.setCharge(400);
					request.setAttribute("buyDetail",buyDetail);
				}else if(evalution <=4 && evalution >3) {
					buyDetail.setCharge(600);
					request.setAttribute("buyDetail",buyDetail);
				}else if(evalution <=5 && evalution >4) {
					buyDetail.setCharge(800);
					request.setAttribute("buyDetail",buyDetail);
				}else if(evalution == 5) {
					buyDetail.setCharge(1000);
					request.setAttribute("buyDetail",buyDetail);
				}

			}

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Cart_buy.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
