package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Delivary;
import dao.AreaDao;
import dao.CategoryDao;
import dao.DelivaryDao;

/**
 * Servlet implementation class Delivary_delete
 */
@WebServlet("/Delivary_delete")
public class Delivary_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索カテゴリーの取得
		HttpSession session = request.getSession();
		CategoryDao category = new CategoryDao();
		session.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			request.setAttribute("area", area.findAll());

		//TODO 配送業者詳細情報を取得
			String delivary_id = request.getParameter("delivary_id");
			int delivaryId = Integer.parseInt(delivary_id);
			DelivaryDao delivaryDao = new DelivaryDao();
			Delivary delivaryData = delivaryDao.DelivaryDetail(delivaryId);
			request.setAttribute("delivaryData", delivaryData);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_delete.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");
			HttpSession session = request.getSession();

			String delivary_id = request.getParameter("delivary_id");
			int delivaryId = Integer.parseInt(delivary_id);

			DelivaryDao delivaryDao = new DelivaryDao();
			delivaryDao.delivaryDelete(delivaryId);
			session.removeAttribute("delivaryInfo");
			response.sendRedirect("Index");
	}
}
