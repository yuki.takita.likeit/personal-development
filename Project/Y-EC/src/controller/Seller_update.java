package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Seller;
import dao.AreaDao;
import dao.CategoryDao;
import dao.SellerDao;

/**
 * Servlet implementation class Seller_update
 */
@WebServlet("/Seller_update")
public class Seller_update extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Seller_update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("sellerInfo")==null) {
				response.sendRedirect("Index");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			session.setAttribute("area", area.findAll());

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Seller_update.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("utf-8");

		HttpSession session = request.getSession();

		//TODO 更新レコードの取得
			String login_id = request.getParameter("seller_login_id");
			String password = request.getParameter("seller_password");
			String password_conf = request.getParameter("seller_password_conf");
			String name = request.getParameter("seller_name");
			String address = request.getParameter("seller_address_name");
			AreaDao areaDao = new AreaDao();
			int addressId = areaDao.findByAreaId(address).getArea_id();
			Seller seller = (Seller)session.getAttribute("sellerInfo");
			String seller_login_id = seller.getSeller_login_id();

			if(!(password.equals(password_conf))) {
				request.setAttribute("errMsg", "パスワードとパスワード(確認)が異なります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Seller_update.jsp");
				dispacher.forward(request, response);
				return;
			}

		//TODO DBに登録
			SellerDao sellerDao = new SellerDao();
			int count = sellerDao.sellerUpdate(login_id, password, name, addressId,seller_login_id);

		//TODO errMsgのセット
			if(count == 0) {
				request.setAttribute("errMsg", "入力データに誤りがあります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Seller_update.jsp");
				dispacher.forward(request, response);
				return;
			}else if(count >= 1) {
				Seller sellerInfo = sellerDao.findByInfo(login_id, password);
				session.setAttribute("sellerInfo", sellerInfo);
				response.sendRedirect("Item_list");
			}
	}
}
