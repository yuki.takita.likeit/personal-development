package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Seller;
import dao.CategoryDao;
import dao.SellerDao;

/**
 * Servlet implementation class Seller_login
 */
@WebServlet("/Seller_login")
public class Seller_login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Seller_login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("sellerInfo")!=null) {
				response.sendRedirect("Item_list");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Seller_login.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO リクエストパラメータの取得
		String loginId = request.getParameter("seller_login_id");
		String password = request.getParameter("seller_password");
		SellerDao sellerDao = new SellerDao();

	//TODO DBにlogin_id,passwordで検索
		Seller seller = sellerDao.findByInfo(loginId, password);

	//TODO findByinfoでデータがない場合はエラーメッセ時をfowardする
		if(seller == null) {
			request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
			RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Seller_login.jsp");
			dispacher.forward(request, response);
			return;
		}

	//TODO セッションにログインデータをセットする
		HttpSession session = request.getSession();
		session.setAttribute("sellerInfo", seller);

	//TODO UserListServletにリダイレクトさせる
		response.sendRedirect("Item_list");
	}

}
