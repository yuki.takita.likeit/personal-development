package controller;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Item;
import beans.Seller;
import dao.AreaDao;
import dao.CategoryDao;
import dao.ItemDao;
import dao.SellerDao;

/**
 * Servlet implementation class Item_list
 */
@WebServlet("/Item_list")
public class Item_list extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Item_list() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがある場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("sellerInfo")==null) {
				response.sendRedirect("Index");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			request.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			request.setAttribute("area", area.findAll());

		//TODO 販売者データの取得
			Seller sellerInfo = (Seller)session.getAttribute("sellerInfo");
			SellerDao sellerDao = new SellerDao();
			Seller seller = sellerDao.findBySellerData(sellerInfo.getSeller_login_id());
			request.setAttribute("seller", seller);

		//TODO 販売商品データの取得
			Seller sellerId = sellerDao.findById(sellerInfo.getSeller_login_id());
			ItemDao itemDao = new ItemDao();
			ArrayList<Item> itemList = new ArrayList<Item>();
			itemList.addAll(itemDao.findByItemSellerId(sellerId.getSeller_id()));
			request.setAttribute("itemList", itemList);

		//TODO 販売数、売り上げ
			ArrayList<Item> itemData = itemDao.buyItemData(seller.getSeller_id());
			int earnings = 0;
			for(Item i :itemData) {
				earnings += i.getItem_price();
			}
			NumberFormat nfCur = NumberFormat.getCurrencyInstance();
			request.setAttribute("earnings", nfCur.format(earnings));


		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Item_list.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
