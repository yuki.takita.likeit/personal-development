package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyData;
import dao.BuyDao;
import dao.CategoryDao;

/**
 * Servlet implementation class User_data_detail
 */
@WebServlet("/User_data_detail")
public class User_data_detail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_data_detail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("userInfo")==null) {
				response.sendRedirect("IndexServret");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO buyIdをゲットパラメータで取得
			String buy_id = request.getParameter("buy_id");
			int buyId = Integer.parseInt(buy_id);
		//TODO 購入詳細データの取得
			BuyDao buyDao = new BuyDao();
			BuyData buyList = buyDao.finBuyId(buyId);
			request.setAttribute("buyData",buyList );

			ArrayList<BuyData> itemList = buyDao.finBuyIdItem(buyId);
			request.setAttribute("itemData",itemList );

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User_data_detail.jsp");
			dispatcher.forward(request, response);
		}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
