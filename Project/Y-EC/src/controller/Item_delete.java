package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Item;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Item_delete
 */
@WebServlet("/Item_delete")
public class Item_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Item_delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("sellerInfo")==null) {
				response.sendRedirect("Index");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO ユーザーデータの取得
			Item item = new Item();
			item.setItem_id(Integer.parseInt(request.getParameter("item_id")));
			ItemDao itemDao = new ItemDao();
			Item itemData = itemDao.findByItem(item.getItem_id());
			session.setAttribute("itemData", itemData);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Item_delete.jsp");
			dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");
			HttpSession session = request.getSession();

			String item_id = request.getParameter("item_id");
			int itemId = Integer.parseInt(item_id);
			ItemDao itemDao = new ItemDao();

		//TODO 商品を削除して商品一覧にリダイレクト
			itemDao.itemDelete(itemId);
			response.sendRedirect("item_list");
	}

}
