package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.Delivary;
import dao.AreaDao;
import dao.CategoryDao;
import dao.DelivaryDao;

/**
 * Servlet implementation class Delivary_update
 */
@WebServlet("/Delivary_update")
public class Delivary_update extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("delivaryInfo")==null) {
			response.sendRedirect("IndexServret");
			return;
		}
	//TODO 検索カテゴリーの取得
		CategoryDao category = new CategoryDao();
		session.setAttribute("category", category.findAll());

	//TODO 地域データの取得
		AreaDao area = new AreaDao();
		session.setAttribute("area", area.findAll());

	//TODO 配送業者詳細情報を取得
		String delivary_id = request.getParameter("delivary_id");
		int delivaryId = Integer.parseInt(delivary_id);
		DelivaryDao delivaryDao = new DelivaryDao();
		Delivary delivaryData = delivaryDao.DelivaryDetail(delivaryId);
		request.setAttribute("delivaryData", delivaryData);


	//TODO フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("utf-8");

		HttpSession session = request.getSession();

		//TODO 更新レコードの取得
			String login_id = request.getParameter("delivary_login_id");
			String password = request.getParameter("delivary_password");
			String password_conf = request.getParameter("delivary_password_conf");
			String name = request.getParameter("delivary_company_name");
			String address = request.getParameter("delivary_address");
			String area_address = request.getParameter("delivary_area_address");
			String sellesText = request.getParameter("delivary_selles_text");
			AreaDao areaDao = new AreaDao();
			int addressId = areaDao.findByAreaId(address).getArea_id();
			int areaId = areaDao.findByAreaId(area_address).getArea_id();

			String delivary_id = request.getParameter("delivary_id");
			int delivaryId = Integer.parseInt(delivary_id);
			Part imgFile = request.getPart("delivary_img");
		//TODO 画像ファイル名を取得
			String imgName = imgFile.getName();

			if(!(password.equals(password_conf))) {
				request.setAttribute("errMsg", "パスワードとパスワード(確認)が異なります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_update.jsp");
				dispacher.forward(request, response);
				return;
			}

		//TODO DBに登録
			DelivaryDao delivaryDao = new DelivaryDao();
			int count = delivaryDao.delivaryUpdate(login_id, password, name, addressId, areaId, sellesText, imgName, delivaryId);

		//TODO errMsgのセット
			if(count == 0) {
				request.setAttribute("errMsg", "入力データに誤りがあります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_update.jsp");
				dispacher.forward(request, response);
				return;
			}else if(count >= 1) {
				Delivary delivaryInfo = delivaryDao.findByInfo(login_id, password);
				session.setAttribute("delivaryInfo", delivaryInfo);
				response.sendRedirect("Delivary_company_list");
			}
	}
}
