package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Delivary;
import dao.CategoryDao;
import dao.DelivaryDao;

/**
 * Servlet implementation class delivary_company_login
 */
@WebServlet("/Delivary_company_login")
public class Delivary_company_login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_company_login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("delivaryInfo")!=null) {
				response.sendRedirect("Delivary_company_list");
				return;
			}

		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_company_login.jsp");
			dispatcher.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO リクエストパラメータの取得
			String loginId = request.getParameter("delivary_login_id");
			String password = request.getParameter("delivary_password");
			DelivaryDao delivaryDao = new DelivaryDao();

		//TODO DBにlogin_id,passwordで検索
			Delivary delivary = delivaryDao.findByInfo(loginId, password);

		//TODO findByinfoでデータがない場合はエラーメッセ時をfowardする
			if(delivary == null) {
				request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_company_login.jsp");
				dispacher.forward(request, response);
				return;
			}

		//TODO セッションにログインデータをセットする
			HttpSession session = request.getSession();
			session.setAttribute("delivaryInfo", delivary);

		//TODO UserListServletにリダイレクトさせる
			response.sendRedirect("Delivary_company_list");
	}
}
