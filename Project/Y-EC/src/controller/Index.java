package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.Item;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//1ページに表示する商品数
		final static int PAGE_MAX_ITEM_COUNT = 8;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがある場合indexServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("itemIndex")!=null) {
			session.removeAttribute("itemIndex");
		}
		if(session.getAttribute("itemSerch")!=null) {
			session.removeAttribute("itemSerch");
		}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			ArrayList<Category> categoryAll = category.findAll();
			session.setAttribute("category", categoryAll);


		//TODO カテゴリー数でカルーセルのオーダー数をセットする。
			int count = category.findcount();
			ArrayList<Integer> countList = new ArrayList<Integer>();
			for(int i = 0 ; i <= count ; i++) {
				countList.add(i);
			}
			request.setAttribute("countList", countList);


		//TODO 商品情報取得、販売個数でソート
			ItemDao itemDao = new ItemDao();
			ArrayList<Item> itemList = new ArrayList<Item>();
			for(Category i : categoryAll ) {
				itemList.addAll(itemDao.indexItem(i.getCategory_id()));
			}
			session.setAttribute("itemIndex", itemList);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Index.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索結果がある場合は削除する
			HttpSession session = request.getSession();
			if(session.getAttribute("itemSerch")!=null) {
				session.removeAttribute("itemSerch");
			}
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO リクエストパラメータの取得
			String categoryName = request.getParameter("category");
			String itemName = request.getParameter("itemSerch");
			CategoryDao categoryDao = new CategoryDao();
			ItemDao itemDao = new ItemDao();
			Category categoryId = categoryDao.findByCategoryId(categoryName);

			ArrayList<Item> itemSerch = itemDao.itemSerch(categoryId.getCategory_id(), itemName);
			session.setAttribute("itemSerch", itemSerch);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Index.jsp");
			dispatcher.forward(request, response);
	}
}
