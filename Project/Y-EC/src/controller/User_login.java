package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.CategoryDao;
import dao.UserDao;

/**
 * Servlet implementation class User_login
 */
@WebServlet("/User_login")
public class User_login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがある場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("userInfo")!=null) {
				response.sendRedirect("User_data");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_login.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO リクエストパラメータの取得
			String loginId = request.getParameter("user_login_id");
			String password = request.getParameter("user_password");
			UserDao userDao = new UserDao();

		//TODO DBにlogin_id,passwordで検索
			User user = userDao.findByInfo(loginId, password);

		//TODO findByinfoでデータがない場合はエラーメッセ時をfowardする
			if(user==null) {
				request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/user_login.jsp");
				dispacher.forward(request, response);
				return;
			}

		//TODO 商品詳細から遷移してきた場合ログイン後もとの商品詳細に戻る
			HttpSession session = request.getSession();
			session.setAttribute("userInfo", user);
			if(session.getAttribute("itemId")!=null) {
				int itemId = (int) session.getAttribute("itemId");
				request.setAttribute("item_id", itemId);
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Item_index_detail.jsp");
				dispacher.forward(request, response);
				return;
			}
		//TODO User_dataにリダイレクトさせる
			response.sendRedirect("User_data");
	}

}
