package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.CategoryDao;
import dao.UserDao;

/**
 * Servlet implementation class User_detail
 */
@WebServlet("/User_detail")
public class User_detail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_detail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("userInfo")==null) {
				response.sendRedirect("Index");
				return;
			}

		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO ユーザーデータの取得
			UserDao userDao = new UserDao();
			User user =  (User) session.getAttribute("userInfo");
			User userDetail = userDao.findByUserDetail(user.getUser_login_id());
			session.setAttribute("userDetail", userDetail);

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User_detail.jsp");
			dispatcher.forward(request, response);
		}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
