package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.AreaDao;
import dao.CategoryDao;
import dao.FavoriteDao;
import dao.UserDao;

/**
 * Servlet implementation class User_sign_up
 */
@WebServlet("/User_sign_up")
public class User_sign_up extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_sign_up() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//TODO 検索カテゴリーの取得
			HttpSession session = request.getSession();
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			session.setAttribute("area", area.findAll());

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User_sign_up.jsp");
			dispatcher.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO 新規登録レコードの取得
			String login_id = request.getParameter("user_login_id");
			String password = request.getParameter("user_password");
			String password_conf = request.getParameter("user_password_conf");
			String name = request.getParameter("user_name");
			String address = request.getParameter("user_address_name");
			String birth_date = request.getParameter("user_birth_date");
			AreaDao areaDao = new AreaDao();
			int addressId = areaDao.findByAreaId(address).getArea_id();

			if(!(password.equals(password_conf))) {
				request.setAttribute("errMsg", "パスワードとパスワード(確認)が異なります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/User_sign_up.jsp");
				dispacher.forward(request, response);
				return;
			}

		//TODO DBに新規登録
			UserDao userDao = new UserDao();
			int count = userDao.userSignUp(login_id, password, name, addressId, birth_date);

		//TODO errMsgのセット
			HttpSession session = request.getSession();
			if(count == 0) {
				request.setAttribute("errMsg", "入力データに誤りがあります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/User_sign_up.jsp");
				dispacher.forward(request, response);
				return;
			}else if(count >= 1) {
				User user = userDao.findByInfo(login_id, password);
				//TODO favorite_tableの作成
				FavoriteDao favoriteDao = new FavoriteDao();
				favoriteDao.FavoriteTableSignUp(user.getUser_id());
				session.setAttribute("userInfo", user);
				response.sendRedirect("User_data");
				return;
			}
	}
}
