package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Area;
import beans.Buy;
import beans.BuyDetail;
import beans.Cart;
import beans.Delivary;
import beans.DelivaryMethod;
import beans.Item;
import beans.User;
import dao.AreaDao;
import dao.BuyDao;
import dao.CartDao;
import dao.CategoryDao;
import dao.DelivaryDao;
import dao.DelivaryMethodDao;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class Cart_buy_detail
 */
@WebServlet("/Cart_buy_detail")
public class Cart_buy_detail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart_buy_detail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	//TODO ログインセッションがある場合indexServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("Index");
			return;
			}

	//TODO ユーザーデータの取得
		UserDao userDao = new UserDao();
		User user =  (User) session.getAttribute("userInfo");
		User userData = userDao.findByUserData(user.getUser_id());
		request.setAttribute("userData", userData);

	//TODO カート内アイテムデータ取得
		CartDao cartDao = new CartDao();
		Cart cart = new Cart();
		cart = cartDao.CartFindUserId(user.getUser_id());
		ArrayList<Cart> cartList = cartDao.CartItemFindUserId(cart.getCart_id());
		request.setAttribute("cartList", cartList);

	//TODO 配送業者情報取得
		String delivary_id = request.getParameter("delivary_id");
		int delivaryId = Integer.parseInt(delivary_id);
		DelivaryDao delivaryDao = new DelivaryDao();
		Delivary delivary = delivaryDao.DelivaryDetail(delivaryId);
		request.setAttribute("delivary", delivary);

	//TODO 配送情報取得
		String method = request.getParameter("method");
		int method_id = Integer.parseInt(method);
		DelivaryMethodDao delivaryMethodDao = new DelivaryMethodDao();
		DelivaryMethod delivaryMethod = delivaryMethodDao.findByDelivaryMethodId(method_id);
		request.setAttribute("delivaryMethod", delivaryMethod);

	//TODO 合計金額のセット
		int totalPrice = 0;
		ArrayList<Integer> cart_price  = cartDao.CartItemFindItemPrice(cart.getCart_id());
		Iterator<Integer> itelator = cart_price.iterator();
		while(itelator.hasNext()) {
			totalPrice += itelator.next();
		}
		int methodPrice = delivaryMethod.getDelivary_method_price();
		String charg = request.getParameter("charg");
		int chargInt = Integer.parseInt(charg);
		totalPrice += methodPrice;
		totalPrice += chargInt;
		BuyDetail buyDetail = new BuyDetail();
		double tax = totalPrice*0.1;
		buyDetail.setTax((int) Math.ceil(tax));
		buyDetail.setCharge(chargInt);
		totalPrice += buyDetail.getTax();
		buyDetail.setTotal_price(totalPrice);

		request.setAttribute("buyDetail",buyDetail);

	//TODO 検索カテゴリーの取得
		CategoryDao category = new CategoryDao();
		request.setAttribute("category", category.findAll());


	//TODO フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Cart_buy_detail.jsp");
		dispatcher.forward(request, response);
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");
			HttpSession session = request.getSession();

		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			request.setAttribute("category", category.findAll());

		//TODO ユーザーデータの取得
			UserDao userDao = new UserDao();
			User user =  (User) session.getAttribute("userInfo");
			User userData = userDao.findByUserData(user.getUser_id());

		//TODO カート内アイテムデータ取得
			CartDao cartDao = new CartDao();
			Cart cart = new Cart();
			cart = cartDao.CartFindUserId(user.getUser_id());

		//TODO 配送業者情報取得
			DelivaryDao delivaryDao = new DelivaryDao();
			String delivary_id = request.getParameter("delivary_id");
			int delivaryId = Integer.parseInt(delivary_id);
			Delivary delivary = delivaryDao.DelivaryDetail(delivaryId);
			AreaDao areaDao = new AreaDao();
			Area area = areaDao.findByAreaId(delivary.getDelivary_area_name());

		//TODO totalPrice
			String total_price = request.getParameter("totalPrice");
			int totalPrice = Integer.parseInt(total_price);

		//TODO method
			String method = request.getParameter("method");



		//TODO 在庫数が足りているのかチェックする。
			ItemDao itemDao = new ItemDao();
			//同じ数字のカウント
			Map<Integer,Integer> stockCheck = new HashMap<Integer,Integer>();

			ArrayList<Integer> cartItemId = cartDao.CartItemFindItemId(cart.getCart_id());
			//TODO itemIdが0のデータを削除
			cartItemId.remove(0);

			//TODO 重複itemIdの個数
				for(Integer i : cartItemId) {
					// marge(key,挿入値,挿入操作方法の指定)
					stockCheck.merge(i, 1, Integer::sum);
				}
				stockCheck.remove(0);
				//TODO stockCheck(itemId,buyCount)在庫数と照らし合わせて足りない場合はエラーメッセージをセットする
				for(Map.Entry<Integer, Integer> entry : stockCheck.entrySet()) {
					//TODO 在庫数と参照
					if(entry.getValue() <= (itemDao.findByItem(entry.getKey())).getItem_stock()) {
						int stock = itemDao.findByItem(entry.getKey()).getItem_stock();
						int setStock = stock - entry.getValue();
						itemDao.itemUpdateStock(entry.getKey(), setStock);
						continue;
					}else {
						//TODO 在庫数以上の場合は在庫数を表示させて、在庫数以上の個数をカートから削除する。
						Item item = itemDao.findByItem(entry.getKey());
						String errMsg = "商品名:" + item.getItem_name() + "の在庫数:" + item.getItem_stock() + "です.";
						request.setAttribute("errMsg", errMsg);
						int deleteCount = entry.getValue() - item.getItem_stock();
						cartDao.cartItemCountDelete(entry.getKey(), deleteCount);
						response.sendRedirect("Cart_buy");
						return;
					}
				}


		//TODO Cart_tableに新規登録
			BuyDao buyDao = new BuyDao();
			//TODO buy_tableに登録
			int count  = buyDao.buySignUp(userData.getUser_id(),method,area.getArea_id(),totalPrice, delivaryId);

		//TODO 購入登録後buy_detail_tableに登録する
			if(count >= 1) {

				ArrayList<Integer> cart_item  = cartDao.CartItemFindItemId(cart.getCart_id());
				Iterator<Integer> itelator = cart_item.iterator();
				Buy buy = buyDao.finBuyUserId(user.getUser_id());
				while(itelator.hasNext()) {
					//TODO buy_detail_table に登録
					buyDao.buyDetailSignUp(buy.getBuy_id(),user.getUser_id(),itelator.next());
					continue;
				}
				Iterator<Integer> itelatorCount = cart_item.iterator();
				while(itelatorCount.hasNext()) {
					itemDao.itemCountAdd(itelatorCount.next());
					continue;
				}
			}

		//TODO cart_itemを削除する
			cartDao.CartItemDelete(cart.getCart_id());

		//TODO 購入データの取得
			request.setAttribute("buyData", buyDao.finBuyData(user.getUser_id()));


		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User_data.jsp");
			dispatcher.forward(request, response);
	}
}