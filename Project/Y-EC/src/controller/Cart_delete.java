package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.CartDao;

/**
 * Servlet implementation class Cart_delete
 */
@WebServlet("/Cart_delete")
public class Cart_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart_delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("utf-8");

		HttpSession session = request.getSession();
		//TODO カート内商品を削除する
		String item_id = request.getParameter("item_id");
		int itemId = Integer.parseInt(item_id);
		User user = (User)session.getAttribute("userInfo");

		//TODO ユーザーIDで検索し商品を削除する
		CartDao cartDao = new CartDao();
		cartDao.cartItemDelete(user.getUser_id(), itemId);

		session.removeAttribute("cartList");
		response.sendRedirect("Cart_create");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
