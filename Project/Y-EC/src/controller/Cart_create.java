package controller;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Cart;
import beans.Item;
import beans.User;
import dao.CartDao;
import dao.CategoryDao;
import dao.UserDao;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/Cart_create")
public class Cart_create extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart_create() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO 検索カテゴリーの取得
			HttpSession session = request.getSession();
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

			Item item = new Item();
			Cart cart = new Cart();
			CartDao cartDao = new CartDao();
			UserDao userDao = new UserDao();
			Object item_id = request.getParameter("itemId");


			if(session.getAttribute("userInfo")==null) {
				//TODO ユーザーログインセッションがない場合はログインセッションに遷移
				request.setAttribute("errMsg", "ログインセッションがありませんログインしてください");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_login.jsp");
				dispatcher.forward(request, response);
				return;

			}else if(item_id != null) {
				//TODO ログインした状態で商品をカートに入れた場合
				item.setItem_id(Integer.parseInt(request.getParameter("itemId")));
				User user  = (User)session.getAttribute("userInfo");

					if(cartDao.CartFindUserId(user.getUser_id())==null) {
						//TODO カートテーブルがない場合は作成しcart_itemに登録
						cartDao.CartCreate(user.getUser_id());
						cart = cartDao.CartFindUserId(user.getUser_id());
						cartDao.CartItemSignUp(cart.getCart_id(), item.getItem_id());

						//TODO カート内アイテムデータ取得
						cart = cartDao.CartFindUserId(user.getUser_id());
						ArrayList<Cart> cartList = cartDao.CartItemFindUserId(cart.getCart_id());
						session.setAttribute("cartList", cartList);

						//TODO カート内合計金額の取得
						int totalPrice = 0;
						ArrayList<Integer> cart_price  = cartDao.CartItemFindItemPrice(cart.getCart_id());
						Iterator<Integer> itelator = cart_price.iterator();
						while(itelator.hasNext()) {
							totalPrice += itelator.next();
						}
						NumberFormat totalPriceFormat = NumberFormat.getCurrencyInstance();
						String totalPriceFormatJpen = totalPriceFormat.format(totalPrice);
						request.setAttribute("totalPrice", totalPriceFormatJpen);

						//TODO ユーザー情報のセット
						User userData = userDao.findByUserData(user.getUser_id());
						request.setAttribute("userData",userData);

						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Cart_create.jsp");
						dispatcher.forward(request, response);
						return;
					}else {
						cart = cartDao.CartFindUserId(user.getUser_id());
						cartDao.CartItemSignUp(cart.getCart_id(), item.getItem_id());

						//TODO カート内アイテムデータ取得
						cart = cartDao.CartFindUserId(user.getUser_id());
						ArrayList<Cart> cartList = cartDao.CartItemFindUserId(cart.getCart_id());
						session.setAttribute("cartList", cartList);

						//TODO カート内合計金額の取得
						int totalPrice = 0;
						ArrayList<Integer> cart_price  = cartDao.CartItemFindItemPrice(cart.getCart_id());
						Iterator<Integer> itelator = cart_price.iterator();
						while(itelator.hasNext()) {
							totalPrice += itelator.next();
						}
						NumberFormat totalPriceFormat = NumberFormat.getCurrencyInstance();
						String totalPriceFormatJpen = totalPriceFormat.format(totalPrice);
						request.setAttribute("totalPrice", totalPriceFormatJpen);

						//TODO ユーザー情報のセット
						User userData = userDao.findByUserData(user.getUser_id());
						request.setAttribute("userData",userData);

						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Cart_create.jsp");
						dispatcher.forward(request, response);
						return;
					}
			}else if(item_id == null) {
				//TODO カートに直接遷移した場合
				User user  = (User)session.getAttribute("userInfo");

				if(cartDao.CartFindUserId(user.getUser_id())==null) {
					//TODO カートテーブルがない場合は作成しcart_itemに登録
					cartDao.CartCreate(user.getUser_id());
					cart = cartDao.CartFindUserId(user.getUser_id());

					//TODO カート内アイテムデータ取得
					cart = cartDao.CartFindUserId(user.getUser_id());
					ArrayList<Cart> cartList = cartDao.CartItemFindUserId(cart.getCart_id());
					session.setAttribute("cartList", cartList);

					//TODO ユーザー情報のセット
					User userData = userDao.findByUserData(user.getUser_id());
					request.setAttribute("userData",userData);

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Cart_create.jsp");
					dispatcher.forward(request, response);
					return;
				}else {
					cart = cartDao.CartFindUserId(user.getUser_id());
					cartDao.CartItemSignUp(cart.getCart_id(), item.getItem_id());

					//TODO カート内アイテムデータ取得
					cart = cartDao.CartFindUserId(user.getUser_id());
					ArrayList<Cart> cartList = cartDao.CartItemFindUserId(cart.getCart_id());
					session.setAttribute("cartList", cartList);

					//TODO カート内合計金額の取得
					int totalPrice = 0;
					ArrayList<Integer> cart_price  = cartDao.CartItemFindItemPrice(cart.getCart_id());
					Iterator<Integer> itelator = cart_price.iterator();
					while(itelator.hasNext()) {
						totalPrice += itelator.next();
					}
					NumberFormat totalPriceFormat = NumberFormat.getCurrencyInstance();
					String totalPriceFormatJpen = totalPriceFormat.format(totalPrice);
					request.setAttribute("totalPrice", totalPriceFormatJpen);

					//TODO ユーザー情報のセット
					User userData = userDao.findByUserData(user.getUser_id());
					request.setAttribute("userData",userData);

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Cart_create.jsp");
					dispatcher.forward(request, response);
					return;
				}
			}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
