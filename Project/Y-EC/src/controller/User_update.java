package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.AreaDao;
import dao.CategoryDao;
import dao.UserDao;

/**
 * Servlet implementation class User_update
 */
@WebServlet("/User_update")
public class User_update extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合indexServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("userInfo")==null) {
				response.sendRedirect("IndexServret");
				return;
			}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO 地域データの取得
			AreaDao area = new AreaDao();
			session.setAttribute("area", area.findAll());

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User_update.jsp");
			dispatcher.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("utf-8");

		HttpSession session = request.getSession();

		//TODO 更新レコードの取得
			String login_id = request.getParameter("user_login_id");
			String password = request.getParameter("user_password");
			String password_conf = request.getParameter("user_password_conf");
			String name = request.getParameter("user_name");
			String address = request.getParameter("user_address_name");
			String birth_date = request.getParameter("user_birth_date");
			AreaDao areaDao = new AreaDao();
			int addressId = areaDao.findByAreaId(address).getArea_id();
			User user = (User)session.getAttribute("userInfo");
			int user_id = user.getUser_id();

			if(!(password.equals(password_conf))) {
				request.setAttribute("errMsg", "パスワードとパスワード(確認)が異なります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/User_update.jsp");
				dispacher.forward(request, response);
				return;
			}

		//TODO DBに登録
			UserDao userDao = new UserDao();
			int count = userDao.userUpdate(login_id, password, name, addressId, birth_date,user_id);

		//TODO errMsgのセット
			if(count == 0) {
				request.setAttribute("errMsg", "入力データに誤りがあります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/User_update.jsp");
				dispacher.forward(request, response);
				return;
			}else if(count >= 1) {
				User userInfo = userDao.findByInfo(login_id, password);
				session.setAttribute("userInfo", userInfo);
				response.sendRedirect("User_data");
			}
	}
}
