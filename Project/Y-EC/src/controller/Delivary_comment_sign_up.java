package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyData;
import beans.DelivaryComment;
import dao.BuyDao;
import dao.CategoryDao;
import dao.DelivaryCommentDao;

/**
 * Servlet implementation class Delivary_comment_sign_up
 */
@WebServlet("/Delivary_comment_sign_up")
public class Delivary_comment_sign_up extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delivary_comment_sign_up() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがある場合indexServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("Index");
			return;
		}
		//TODO 検索カテゴリーの取得
			CategoryDao category = new CategoryDao();
			session.setAttribute("category", category.findAll());

		//TODO buyIdをゲットパラメータで取得
			String buy_id = request.getParameter("buy_id");
			int buyId = Integer.parseInt(buy_id);

		//TODO 購入詳細データの取得
			BuyDao buyDao = new BuyDao();
			BuyData buyList = buyDao.finBuyId(buyId);
			session.setAttribute("buyData",buyList );

		//TODO フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_comment_sign_up.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
			request.setCharacterEncoding("utf-8");

		//TODO 入力データの取得
			String buy_id = request.getParameter("buy_id");
			int buyId = Integer.parseInt(buy_id);
			String select = request.getParameter("select");
			String comment = request.getParameter("comment");
			BuyDao buyDao = new BuyDao();
			BuyData buyList = buyDao.finBuyIdComment(buyId);

		DelivaryCommentDao delivaryCommentDao = new DelivaryCommentDao();
		DelivaryComment commentCountInt = delivaryCommentDao.findCommentBuyId(buyId);
		if(commentCountInt != null) {
			request.setAttribute("errMsg", "すでに口コミを登録済みです");
			RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_comment_sign_up.jsp");
			dispacher.forward(request, response);
			return;
		}

		//TODO 口コミ新規登録
			DelivaryCommentDao Dc = new DelivaryCommentDao();
			int count = Dc.commentSignUp(buyList.getBuy_delivary_id(), buyList.getBuy_id(), buyList.getBuy_user_id(), buyList.getDelivary_method_id(), select, comment);
			if(count > 1) {
				request.setAttribute("errMsg", "入力データに誤りがあります");
				RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_comment_sign_up.jsp");
				dispacher.forward(request, response);
				return;
			}else if(count >= 1) {
				//TODO 口コミが新規登録された場合配送業者の評価も更新する
					//TODO 評価をすべて足して総数で割る
					BuyData delivaryId = buyDao.finBuyIdComment(buyList.getBuy_id());
					ArrayList<Integer> comentList = Dc.findEvalution(delivaryId.getBuy_delivary_id());
					Iterator<Integer> iterator = comentList.iterator();
					int commentCount = 0;
					while(iterator.hasNext()) {
						commentCount += iterator.next();
					}
			int avaCountComment = commentCount/comentList.size();
			Dc.evalution(delivaryId.getBuy_delivary_id(),avaCountComment);
			ArrayList<DelivaryComment> delivaryList = delivaryCommentDao.findAll();
			request.setAttribute("delivaryList", delivaryList);
			RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/Delivary_comment_list.jsp");
			dispacher.forward(request, response);
			}
	}

}
