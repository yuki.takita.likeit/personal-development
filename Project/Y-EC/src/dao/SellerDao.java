package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Seller;

public class SellerDao {
	public ArrayList<Seller> findAll() {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM seller_table;";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<Seller> seller_list = new ArrayList<Seller>();

			while (rs.next()) {
				Seller seller = new Seller();
				seller.setSeller_id(rs.getInt("seller_id"));
				seller.setSeller_login_id(rs.getString("seller_login_id"));
				seller.setSeller_password(rs.getString("seller_password"));
				seller.setSeller_address(rs.getInt("seller_address"));
				seller_list.add(seller);
			}

			return seller_list;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
		public Seller findByInfo(String login_id,String password) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM seller_table WHERE seller_login_id = ? and seller_password = md5(?);";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			String login_idData = rs.getString("seller_login_id");
			String name = rs.getString("seller_name");
			Seller seller = new Seller(login_idData,name);
			return seller;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public int sellerSignUp(String login_id, String password, String name,int address) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO seller_table (" +
					"seller_login_id," +
					"seller_password," +
					"seller_name," +
					"seller_address," +
					"seller_create_date,"+
					"seller_update_date "+
					 ")VALUES("+
					"?,MD5(?),?,?,cast(now() as datetime),cast(now() as datetime));";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			pstmt.setString(3, name);
			pstmt.setInt(4, address);
			int count = pstmt.executeUpdate();
			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
		public int sellerUpdate(String login_id, String password, String name,int address,String seller_login_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "UPDATE seller_table SET " +
					"seller_login_id = ?," +
					"seller_password = ?," +
					"seller_name = ?," +
					"seller_address = ?," +
					"seller_update_date = cast(now() as datetime) " +
					"WHERE seller_login_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			pstmt.setString(3, name);
			pstmt.setInt(4, address);
			pstmt.setString(5, seller_login_id);
			int count = pstmt.executeUpdate();
			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
		public int sellerDelete(int seller_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "DELETE FROM `seller_table` WHERE seller_id ;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, seller_id);
			int count = pstmt.executeUpdate();
			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
		public Seller findBySellerData(String seller_login_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM seller_table WHERE seller_login_id = ? ";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, seller_login_id);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			Seller seller = new Seller();
			seller.setSeller_id(rs.getInt("seller_id"));
			seller.setSeller_login_id(rs.getString("seller_login_id"));
			seller.setSeller_name(rs.getString("seller_name"));
			seller.setSeller_create_date(rs.getDate("seller_create_date"));
			seller.setSeller_update_date(rs.getDate("seller_update_date"));
			return seller;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public Seller findBySellerDetail(String seller_login_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT "+
					"seller_table.seller_id,"+
					"seller_table.seller_login_id,"+
					"seller_table.seller_name,"+
					"seller_table.seller_address,"+
					"seller_table.seller_create_date,"+
					"seller_table.seller_update_date,"+
					"area_table.area_name_prefectual "+
					"FROM seller_table "+
					"JOIN area_table "+
					"ON seller_table.seller_address = area_table.area_id "+
					"WHERE seller_table.seller_login_id = ?;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, seller_login_id);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			Seller seller = new Seller();
			seller.setSeller_id(rs.getInt("seller_id"));
			seller.setSeller_login_id(rs.getString("seller_login_id"));
			seller.setSeller_name(rs.getString("seller_name"));
			seller.setSeller_address(rs.getInt("seller_address"));
			seller.setSeller_create_date(rs.getDate("seller_create_date"));
			seller.setSeller_update_date(rs.getDate("seller_update_date"));
			seller.setArea_name(rs.getString("area_name_prefectual"));
			return seller;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public Seller findById(String login_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM seller_table WHERE seller_login_id = ?;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			Seller seller = new Seller();
			seller.setSeller_id(rs.getInt("seller_id"));
			return seller;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
