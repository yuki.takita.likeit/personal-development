package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Area;
import beans.User;

	public class AreaDao {
		public ArrayList<Area> findAll() {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM area_table ORDER BY area_id";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<Area> area_list = new ArrayList<Area>();

			while(rs.next()) {
				Area area = new Area();
				area.setArea_id(rs.getInt("area_id"));
				area.setArea_name_prefectual(rs.getString("area_name_prefectual"));
				area_list.add(area);
			}

			return area_list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public Area findByAreaId(String area_name) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM area_table WHERE area_name_prefectual = ? ";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, area_name);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			int areaId = rs.getInt("area_id");
			Area area = new Area(areaId);
			return area;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
