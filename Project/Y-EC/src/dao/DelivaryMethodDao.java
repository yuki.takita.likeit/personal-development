package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.DelivaryMethod;

public class DelivaryMethodDao {

		public ArrayList<DelivaryMethod> findAll() {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM delivary_method ORDER BY delivary_price";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<DelivaryMethod> delivaryMethodList = new ArrayList<DelivaryMethod>();

			while(rs.next()) {
				DelivaryMethod delivaryMethod = new DelivaryMethod();
				delivaryMethod.setDelivary_method_id(rs.getInt("delivary_method_id"));
				delivaryMethod.setDelivary_method_name(rs.getString("delivary_method_name"));
				delivaryMethod.setDelivary_method_price(rs.getInt("delivary_price"));
				delivaryMethodList.add(delivaryMethod);
			}

			return delivaryMethodList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public DelivaryMethod findByDelivaryMethodId(int delivary_method_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM delivary_method WHERE delivary_method_id = ? ";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, delivary_method_id);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			DelivaryMethod delivaryMethod = new DelivaryMethod();
			delivaryMethod.setDelivary_method_id(rs.getInt("delivary_method_id"));
			delivaryMethod.setDelivary_method_name(rs.getString("delivary_method_name"));
			delivaryMethod.setDelivary_method_price(rs.getInt("delivary_price"));
			return delivaryMethod;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
