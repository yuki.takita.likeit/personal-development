package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.DelivaryComment;

public class DelivaryCommentDao {
	public ArrayList<DelivaryComment> findDelivary(String min,String max,int area) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
					"delivary_table.delivary_id," +
					"delivary_table.delivary_name," +
					"delivary_table.delivary_selles_text," +
					"delivary_table.delivary_img_name," +
					"delivary_table.delivary_evaluation," +
					"area.area_id," +
					"area.area_name_prefectual " +
					"FROM delivary_table " +
					"JOIN area_table as area " +
					"ON delivary_table.delivary_area_id = area.area_id " +
					"WHERE (delivary_table.delivary_evaluation BETWEEN ? AND ? )AND area.area_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, min);
			pstmt.setString(2, max);
			pstmt.setInt(3, area);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<DelivaryComment> DCL = new ArrayList<DelivaryComment>();
			while (rs.next()) {
				DelivaryComment delivaryComment = new DelivaryComment();
				delivaryComment.setComment_delivary_id(rs.getInt("delivary_id"));
				delivaryComment.setDelivary_name(rs.getString("delivary_name"));
				delivaryComment.setDelivary_selles_text(rs.getString("delivary_selles_text"));
				delivaryComment.setDelivary_evaluation(rs.getInt("delivary_evaluation"));
				delivaryComment.setArea_name_prefectual(rs.getString("area_name_prefectual"));
				delivaryComment.setDelivary_img_name(rs.getString("delivary_img_name"));


				String sqlComment = "	SELECT * FROM comment_table WHERE comment_delivary_id = ? ORDER BY comment_buy_id DESC;\r\n" +
						"";
				PreparedStatement pstmtC = con.prepareStatement(sqlComment);
				pstmtC.setInt(1, rs.getInt("delivary_id"));
				ResultSet rsC = pstmtC.executeQuery();
				if(!rsC.next()) {}
				delivaryComment.setComment_detail(rsC.getString("comment_detail"));
				delivaryComment.setComment_create_date(rsC.getDate("comment_create_date"));
				DCL.add(delivaryComment);
			}
			return DCL;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public int commentSignUp(int comment_delivary_id,int comment_buy_id,int comment_user_id,
			int comment_method_id,String comment_evalution,String comment_detail) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "INSERT INTO comment_table (" +
				"comment_delivary_id," +
				"comment_buy_id," +
				"comment_user_id," +
				"comment_method_id," +
				"comment_evalution," +
				"comment_detail," +
				"comment_create_date"+
				")VALUES("+
				"?,?,?,?,?,?,cast(now() as datetime));";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, comment_delivary_id);
		pstmt.setInt(2, comment_buy_id);
		pstmt.setInt(3, comment_user_id);
		pstmt.setInt(4, comment_method_id);
		pstmt.setString(5, comment_evalution);
		pstmt.setString(6, comment_detail);

		pstmt.executeUpdate();
		return 1;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public ArrayList<DelivaryComment> findComment(int delivary_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "	SELECT * FROM comment_table WHERE comment_delivary_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, delivary_id);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<DelivaryComment> DCL = new ArrayList<DelivaryComment>();
			while (rs.next()) {
				DelivaryComment delivaryComment = new DelivaryComment();
				delivaryComment.setComment_id(rs.getInt("comment_id"));
				delivaryComment.setComment_delivary_id(rs.getInt("comment_delivary_id"));
				delivaryComment.setComment_buy_id(rs.getInt("comment_buy_id"));
				delivaryComment.setComment_user_id(rs.getInt("comment_user_id"));
				delivaryComment.setComment_method_id(rs.getInt("comment_method_id"));
				delivaryComment.setComment_evalution(rs.getInt("comment_evalution"));
				delivaryComment.setComment_detail(rs.getString("comment_detail"));
				delivaryComment.setComment_create_date(rs.getDate("comment_create_date"));
				DCL.add(delivaryComment);
			}
			return DCL;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public ArrayList<Integer> findEvalution(int delivary_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "	SELECT * FROM comment_table WHERE comment_delivary_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, delivary_id);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<Integer> DCL = new ArrayList<Integer>();
			while (rs.next()) {
				DCL.add(rs.getInt("comment_evalution"));
			}
			return DCL;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public int evalution(int delivary_id,int sumEvalution) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "UPDATE delivary_table SET delivary_evaluation = ? WHERE delivary_id = ?;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, sumEvalution);
		pstmt.setInt(2, delivary_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public DelivaryComment findCommentBuyId(int buy_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM comment_table WHERE comment_buy_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, buy_id);
			ResultSet rs = pstmt.executeQuery();

			if (!rs.next()) {}
				DelivaryComment delivaryComment = new DelivaryComment();
				delivaryComment.setComment_id(rs.getInt("comment_id"));
			return delivaryComment;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public ArrayList<DelivaryComment> findAll() {
			Connection con = null;
			try {
				//DB接続
				con = DBManager.getConnection();

				String sql = "SELECT " +
						"delivary_table.delivary_id," +
						"delivary_table.delivary_name," +
						"delivary_table.delivary_selles_text," +
						"delivary_table.delivary_img_name," +
						"delivary_table.delivary_evaluation," +
						"area.area_id," +
						"area.area_name_prefectual " +
						"FROM delivary_table " +
						"JOIN area_table as area " +
						"ON delivary_table.delivary_area_id = area.area_id ";

				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				ArrayList<DelivaryComment> DCL = new ArrayList<DelivaryComment>();
				while (rs.next()) {
					DelivaryComment delivaryComment = new DelivaryComment();
					delivaryComment.setComment_delivary_id(rs.getInt("delivary_id"));
					delivaryComment.setDelivary_name(rs.getString("delivary_name"));
					delivaryComment.setDelivary_evaluation(rs.getInt("delivary_evaluation"));
					delivaryComment.setArea_name_prefectual(rs.getString("area_name_prefectual"));
					delivaryComment.setDelivary_img_name(rs.getString("delivary_img_name"));


					String sqlComment = "	SELECT * FROM comment_table WHERE comment_delivary_id = ? ORDER BY comment_buy_id DESC;\r\n" +
							"";
					PreparedStatement pstmtC = con.prepareStatement(sqlComment);
					pstmtC.setInt(1, rs.getInt("delivary_id"));
					ResultSet rsC = pstmtC.executeQuery();
					if(!rsC.next()) {}
					delivaryComment.setComment_detail(rsC.getString("comment_detail"));
					delivaryComment.setComment_create_date(rsC.getDate("comment_create_date"));
					DCL.add(delivaryComment);
				}
				return DCL;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
}
