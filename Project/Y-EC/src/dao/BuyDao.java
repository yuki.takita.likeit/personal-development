package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Buy;
import beans.BuyData;

public class BuyDao {

	public ArrayList<BuyData> finBuyData(int user_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
						"buy_table.buy_create_date," +
						"buy_table.delivary_method," +
						"delivary_table.delivary_name," +
						"buy_table.buy_total_price," +
						"buy_table.buy_id "+
						"FROM buy_table " +
						"JOIN delivary_table ON buy_table.buy_delivary_id = delivary_table.delivary_id " +
						"WHERE buy_table.buy_user_id = ? ";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, user_id);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<BuyData> buy_list = new ArrayList<BuyData>();

			while (rs.next()) {
				BuyData buyData = new BuyData();
				buyData.setBuy_create_date(rs.getDate("buy_create_date"));
				buyData.setDelivary_method(rs.getString("delivary_method"));
				buyData.setDelivary_name(rs.getString("delivary_name"));
				buyData.setBuy_total_price(rs.getInt("buy_total_price"));
				buyData.setBuy_id(rs.getInt("buy_id"));
				buy_list.add(buyData);
			}

			return buy_list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public ArrayList<BuyData> finBuyDataDetail(int buy_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
						"buy_table.buy_create_date," +
						"buy_table.delivary_method," +
						"delivary_table.delivary_name," +
						"buy_table.buy_total_price," +
						"buy_table.buy_id "+
						"FROM buy_table " +
						"JOIN delivary_table ON buy_table.buy_delivary_id = delivary_table.delivary_id " +
						"WHERE buy_table.buy_id = ? ";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, buy_id);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<BuyData> buy_list = new ArrayList<BuyData>();

			while (rs.next()) {
				BuyData buyData = new BuyData();
				buyData.setBuy_create_date(rs.getDate("buy_create_date"));
				buyData.setDelivary_method(rs.getString("buy_delivary_method"));
				buyData.setDelivary_name(rs.getString("delivary_name"));
				buyData.setBuy_total_price(rs.getInt("total_price"));
				buyData.setBuy_id(rs.getInt("buy_id"));
				buy_list.add(buyData);
			}

			return buy_list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public BuyData finBuyId(int buy_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
					"buy_detail_table.buy_detail_buy_id," +
					"buy_detail_table.buy_detail_user_id," +
					"buy_detail_table.buy_detail_item_id," +
					"buy_table.delivary_method," +
					"buy_table.delivary_area_id," +
					"buy_table.buy_total_price," +
					"buy_table.buy_create_date," +
					"delivary_table.delivary_name," +
					"item_table.item_name," +
					"item_table.item_price " +
					"FROM buy_detail_table " +
					"JOIN buy_table " +
					"ON buy_detail_table.buy_detail_buy_id = buy_table.buy_id " +
					"JOIN item_table " +
					"ON buy_detail_table.buy_detail_item_id = item_table.item_id " +
					"JOIN delivary_table " +
					"ON buy_table.buy_delivary_id = delivary_table.delivary_id " +
					"WHERE buy_detail_table.buy_detail_buy_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, buy_id);
			ResultSet rs = pstmt.executeQuery();

			if (!rs.next()) {}
				BuyData buyData = new BuyData();
				buyData.setBuy_create_date(rs.getDate("buy_create_date"));
				buyData.setDelivary_method(rs.getString("delivary_method"));
				buyData.setDelivary_name(rs.getString("delivary_name"));
				buyData.setBuy_total_price(rs.getInt("buy_total_price"));
				buyData.setBuy_id(rs.getInt("buy_detail_buy_id"));
			return buyData;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public ArrayList<BuyData> finBuyIdItem(int buy_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
					"buy_detail_table.buy_detail_buy_id," +
					"buy_detail_table.buy_detail_user_id," +
					"buy_detail_table.buy_detail_item_id," +
					"buy_table.delivary_method," +
					"buy_table.delivary_area_id," +
					"buy_table.buy_total_price," +
					"buy_table.buy_create_date," +
					"delivary_table.delivary_name," +
					"item_table.item_name," +
					"item_table.item_price " +
					"FROM buy_detail_table " +
					"JOIN buy_table " +
					"ON buy_detail_table.buy_detail_buy_id = buy_table.buy_id " +
					"JOIN item_table " +
					"ON buy_detail_table.buy_detail_item_id = item_table.item_id " +
					"JOIN delivary_table " +
					"ON buy_table.buy_delivary_id = delivary_table.delivary_id " +
					"WHERE buy_detail_table.buy_detail_buy_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, buy_id);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<BuyData> buy_list = new ArrayList<BuyData>();

			while (rs.next()) {
				BuyData buyData = new BuyData();
				buyData.setItem_name(rs.getString("item_name"));
				buyData.setItem_price(rs.getInt("item_price"));
				buy_list.add(buyData);
			}

			return buy_list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public BuyData finBuyIdComment(int buy_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
					"buy_detail_table.buy_detail_buy_id," +
					"buy_detail_table.buy_detail_user_id," +
					"delivary_table.delivary_id," +
					"delivary_method.delivary_method_id " +
					"FROM buy_detail_table " +
					"JOIN buy_table " +
					"ON buy_detail_table.buy_detail_buy_id = buy_table.buy_id " +
					"JOIN delivary_method " +
					"ON buy_table.delivary_method = delivary_method_name " +
					"JOIN delivary_table " +
					"ON buy_table.buy_delivary_id = delivary_table.delivary_id " +
					"WHERE buy_detail_table.buy_detail_buy_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, buy_id);
			ResultSet rs = pstmt.executeQuery();

			if (!rs.next()) {}
				BuyData buyData = new BuyData();
				buyData.setBuy_user_id(rs.getInt("buy_detail_user_id"));
				buyData.setBuy_id(rs.getInt("buy_detail_buy_id"));
				buyData.setBuy_delivary_id(rs.getInt("delivary_id"));
				buyData.setDelivary_method_id(rs.getInt("delivary_method_id"));

			return buyData;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public int buySignUp(int buy_user_id,String delivary_method,int delivary_area_id,int buy_total_price,int buy_delivary_id) {
		Connection con = null;
	try {
		//DB接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO buy_table(" +
					"buy_user_id," +
					"delivary_method," +
					"delivary_area_id," +
					"buy_total_price," +
					"buy_delivary_id," +
					"buy_create_date" +
					")VALUES(" +
					"?,?,?,?,?,cast(now() as datetime)" +
					")";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, buy_user_id);
			pstmt.setString(2, delivary_method);
			pstmt.setInt(3, delivary_area_id);
			pstmt.setInt(4, buy_total_price);
			pstmt.setInt(5, buy_delivary_id);
			int count = pstmt.executeUpdate();
			if(count >= 1) {

			}
			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public int buyDetailSignUp(int buy_detail_buy_id,int buy_detail_user_id,int buy_detail_item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "INSERT INTO buy_detail_table(" +
				"buy_detail_buy_id," +
				"buy_detail_user_id," +
				"buy_detail_item_id" +
				")VALUES(" +
				"?,?,?" +
				")";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, buy_detail_buy_id);
		pstmt.setInt(2, buy_detail_user_id);
		pstmt.setInt(3, buy_detail_item_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public Buy finBuyUserId(int user_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM buy_table WHERE buy_user_id = ?  ORDER BY buy_id DESC limit 1;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, user_id);
			ResultSet rs = pstmt.executeQuery();

			if (!rs.next()) {
			}
			Buy buy = new Buy();
			buy.setBuy_id(rs.getInt("buy_id"));
			buy.setBuy_user_id(rs.getInt("buy_user_id"));
			buy.setDelivary_method(rs.getString("delivary_method"));
			buy.setDelivary_area_id(rs.getInt("delivary_area_id"));
			buy.setBuy_total_price(rs.getInt("buy_total_price"));
			buy.setDelivary_id(rs.getInt("buy_delivary_id"));
			buy.setBuy_create_date(rs.getDate("buy_create_date"));
			return buy;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
