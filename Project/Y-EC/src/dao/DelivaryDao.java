package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Delivary;

public class DelivaryDao {
	public Delivary findByInfo(String login_id,String password) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM delivary_table WHERE delivary_login_id = ? and delivary_password = md5(?)";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			Delivary delivary = new Delivary();
			delivary.setDelivary_id(rs.getInt("delivary_id"));
			delivary.setDelivary_login_id(rs.getString("delivary_login_id"));
			delivary.setDelivary_name(rs.getString("delivary_name"));
			return delivary;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public int delivarySignUp(String login_id, String password, String name,int address
								,int area_address,String selles_text,String img_name) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO delivary_table (" +
					"delivary_login_id," +
					"delivary_password," +
					"delivary_name," +
					"delivary_address," +
					"delivary_area_id,"+
					"delivary_selles_text,"+
					"delivary_img_name,"+
					"delivary_create_date,"+
					"delivary_update_date "+
					 ")VALUES("+
					"?,MD5(?),?,?,?,?,?,cast(now() as datetime),cast(now() as datetime));";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			pstmt.setString(3, name);
			pstmt.setInt(4, address);
			pstmt.setInt(5, area_address);
			pstmt.setString(6, selles_text);
			pstmt.setString(7, img_name);
			int count = pstmt.executeUpdate();
			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public int delivaryUpdate(String login_id, String password,
			String name,int address_id,int area_id,String selles_text,String img_name,int delivary_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "UPDATE `delivary_table` SET " +
				"`delivary_login_id = ?`," +
				"`delivary_password = ?`," +
				"`delivary_name = ?`," +
				"`delivary_address = ?`," +
				"`delivary_area_id = ?`," +
				"`delivary_selles_text = ?`," +
				"`delivary_img_name = ?`," +
				"`delivary_update_date = cast(now() as datetime)` " +
				"WHERE = ?;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, login_id);
		pstmt.setString(2, password);
		pstmt.setString(3, name);
		pstmt.setInt(4, address_id);
		pstmt.setInt(5, area_id);
		pstmt.setString(6, selles_text);
		pstmt.setString(7, img_name);
		pstmt.setInt(8, delivary_id);
		int count = pstmt.executeUpdate();
		return count;

	} catch (SQLException e) {
		e.printStackTrace();
		return 0;

	} finally {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
}
	public int delivaryDelete(int delivary_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "DELETE FROM delivary_table WHERE delivary_id = ? ;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, delivary_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public ArrayList<Delivary> findAllList() {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT " +
				"delivary_table.delivary_id," +
				"delivary_table.delivary_login_id," +
				"delivary_table.delivary_name," +
				"delivary_table.delivary_address," +
				"delivary_table.delivary_area_id," +
				"delivary_table.delivary_selles_text," +
				"delivary_table.delivary_img_name," +
				"delivary_table.delivary_evaluation," +
				"delivary_table.delivary_create_date," +
				"delivary_table.delivary_update_date," +
				"address.area_name_prefectual as address_name," +
				"area.area_name_prefectual as area_name " +
				"FROM delivary_table " +
				"JOIN area_table as address " +
				"ON delivary_table.delivary_address = address.area_id " +
				"JOIN area_table as area " +
				"ON delivary_table.delivary_area_id = area.area_id;";

		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		ArrayList<Delivary> delivary_list = new ArrayList<Delivary>();

		while (rs.next()) {
			Delivary delivary = new Delivary();
			delivary.setDelivary_id(rs.getInt("delivary_id"));
			delivary.setDelivary_login_id(rs.getString("delivary_login_id"));
			delivary.setDelivary_name(rs.getString("delivary_name"));
			delivary.setDelivary_address(rs.getInt("delivary_address"));
			delivary.setDelivary_area_id(rs.getInt("delivary_area_id"));
			delivary.setDelivary_selles_text(rs.getString("delivary_selles_text"));
			delivary.setDelivary_img_name(rs.getString("delivary_img_name"));
			delivary.setDelivary_evalution(rs.getInt("delivary_evaluation"));
			delivary.setDelivary_create_date(rs.getDate("delivary_create_date"));
			delivary.setDelivary_update_date(rs.getDate("delivary_update_date"));
			delivary.setDelivary_address_name(rs.getString("address_name"));
			delivary.setDelivary_area_name(rs.getString("area_name"));

			delivary_list.add(delivary);
		}

		return delivary_list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public Delivary DelivaryDetail(int delivary_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT " +
				"delivary_table.delivary_id," +
				"delivary_table.delivary_login_id," +
				"delivary_table.delivary_name," +
				"delivary_table.delivary_selles_text," +
				"delivary_table.delivary_img_name," +
				"delivary_table.delivary_evaluation," +
				"delivary_table.delivary_create_date," +
				"delivary_table.delivary_update_date," +
				"address.area_name_prefectual," +
				"area.area_name_prefectual " +
				"FROM delivary_table " +
				"JOIN area_table as address  " +
				"ON delivary_table.delivary_address = address.area_id " +
				"JOIN area_table as area " +
				"ON delivary_table.delivary_area_id = area.area_id " +
				"WHERE delivary_table.delivary_id = ? ;";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, delivary_id);
		ResultSet rs = pstmt.executeQuery();

		//falseの時にnullを返す
		if (!rs.next()) {
			return null;
		}
		Delivary delivary = new Delivary();
		delivary.setDelivary_id(rs.getInt("delivary_id"));
		delivary.setDelivary_login_id(rs.getString("delivary_login_id"));
		delivary.setDelivary_name(rs.getString("delivary_name"));
		delivary.setDelivary_selles_text(rs.getString("delivary_selles_text"));
		delivary.setDelivary_img_name(rs.getString("delivary_img_name"));
		delivary.setDelivary_evalution(rs.getInt("delivary_evaluation"));
		delivary.setDelivary_create_date(rs.getDate("delivary_create_date"));
		delivary.setDelivary_update_date(rs.getDate("delivary_update_date"));
		delivary.setDelivary_address_name(rs.getString("area_name_prefectual"));
		delivary.setDelivary_area_name(rs.getString("area_name_prefectual"));
		return delivary;

	} catch (SQLException e) {
		e.printStackTrace();
		return null;

	} finally {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
}
