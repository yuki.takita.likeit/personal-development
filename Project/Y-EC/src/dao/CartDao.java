package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Cart;

public class CartDao {
	public int CartCreate(int user_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "INSERT INTO cart_table(" +
				"cart_user_id," +
				"cart_create_date " +
				")VALUES(?,cast(now() as datetime));";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, user_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public int CartItemSignUp(int cart_id,int item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "INSERT INTO cart_item(" +
				"cart_id," +
				"cart_item_id,"+
				"cart_item_create_date" +
				")VALUES(?,?,cast(now() as datetime));";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, cart_id);
		pstmt.setInt(2, item_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public Cart CartFindUserId(int user_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT * FROM cart_table WHERE cart_user_id = ? ;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, user_id);
		ResultSet rs = pstmt.executeQuery();

		if(!rs.next()) {
			return null;
		}
		Cart cart = new Cart();
		cart.setCart_user_id(rs.getInt("cart_user_id"));
		cart.setCart_id(rs.getInt("cart_id"));
		cart.setCart_create_date(rs.getDate("cart_create_date"));
		return cart;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public ArrayList<Cart> CartItemFindUserId(int cart_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT " +
				"cart_table.cart_id," +
				"cart_table.cart_user_id," +
				"cart_table.cart_create_date," +
				"cart_item.cart_item_id," +
				"cart_item.cart_item_create_date," +
				"item_table.item_id," +
				"item_table.item_name," +
				"item_table.item_price," +
				"item_table.item_detail,"+
				"item_table.item_stock," +
				"item_table.item_img_name " +
				"FROM cart_table " +
				"JOIN cart_item " +
				"ON cart_table.cart_id = cart_item.cart_id " +
				"JOIN item_table " +
				"ON cart_item.cart_item_id = item_table.item_id " +
				"WHERE cart_table.cart_id = ? ;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, cart_id);
		ResultSet rs = pstmt.executeQuery();

		ArrayList<Cart> cartList = new ArrayList<Cart>();
		while(rs.next()) {
			Cart cart = new Cart();
			cart.setCart_item_id(rs.getInt("cart_item_id"));
			cart.setCart_item_create_date(rs.getDate("cart_item_create_date"));
			cart.setItem_name(rs.getString("item_name"));
			cart.setItem_detail(rs.getString("item_detail"));
			cart.setItem_price(rs.getInt("item_price"));
			cart.setItem_img_name(rs.getString("item_img_name"));
			cart.setCart_item_id(rs.getInt("item_id"));
			cart.setItem_stock(rs.getInt("item_stock"));
			cartList.add(cart);
		}
		return cartList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public int cartItemDelete(int user_id,int item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT * FROM cart_table WHERE cart_user_id = ? ;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, user_id);
		ResultSet rs = pstmt.executeQuery();

		if(!rs.next()) {}
		int cart_id = rs.getInt("cart_id");
		String sqlDelete = "DELETE FROM cart_item WHERE cart_item_id= ? AND cart_id= ? Limit 1;";
		PreparedStatement pstmtD = con.prepareStatement(sqlDelete);
		pstmtD.setInt(1,item_id);
		pstmtD.setInt(2,cart_id);
		int count = pstmtD.executeUpdate();

		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public ArrayList<Integer> CartItemFindItemPrice(int cart_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT " +
				"cart_table.cart_id," +
				"cart_table.cart_user_id," +
				"cart_table.cart_create_date," +
				"cart_item.cart_item_id," +
				"cart_item.cart_item_create_date," +
				"item_table.item_id," +
				"item_table.item_name," +
				"item_table.item_price," +
				"item_table.item_detail,"+
				"item_table.item_img_name " +
				"FROM cart_table " +
				"JOIN cart_item " +
				"ON cart_table.cart_id = cart_item.cart_id " +
				"JOIN item_table " +
				"ON cart_item.cart_item_id = item_table.item_id " +
				"WHERE cart_table.cart_id = ? ;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, cart_id);
		ResultSet rs = pstmt.executeQuery();

		ArrayList<Integer> cartList = new ArrayList<Integer>();
		while(rs.next()) {
			cartList.add(rs.getInt("item_price"));
		}
		return cartList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public ArrayList<Integer> CartItemFindItemId(int cart_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT * FROM cart_item WHERE cart_id = ?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, cart_id);
		ResultSet rs = pstmt.executeQuery();

		ArrayList<Integer> cartList = new ArrayList<Integer>();
		while(rs.next()) {
			Integer cart = rs.getInt("cart_item_id");
			cartList.add(cart);
		}
		return cartList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public int CartItemDelete(int cart_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "DELETE FROM cart_item WHERE cart_id = ?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, cart_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public int cartItemCountDelete(int itemId,int deleteCount) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sqlDelete = "DELETE FROM cart_item WHERE cart_item_id= ?  Limit ?;";
		PreparedStatement pstmtD = con.prepareStatement(sqlDelete);
		pstmtD.setInt(1,itemId);
		pstmtD.setInt(2,deleteCount);
		int count = pstmtD.executeUpdate();

		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
}
