package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Favorite;

public class FavoriteDao {
	public ArrayList<Favorite> findFavorite(int user_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
					"favorite_table.favorite_id," +
					"favorite_table.favorite_user_id," +
					"favorite_item_table.favorite_id," +
					"favorite_item_table.favorite_item_id," +
					"favorite_item_table.favorite_item_create_date," +
					"item_table.item_id," +
					"item_table.item_seller_id," +
					"item_table.item_name," +
					"item_table.item_category_id," +
					"item_table.item_detail," +
					"item_table.item_price," +
					"item_table.item_img_name," +
					"item_table.item_count," +
					"item_table.item_create_date," +
					"item_table.item_stock " +
					"FROM favorite_table " +
					"JOIN favorite_item_table " +
					"ON favorite_table.favorite_id = favorite_item_table.favorite_id " +
					"JOIN item_table " +
					"ON favorite_item_table.favorite_item_id = item_table.item_id " +
					"WHERE favorite_user_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, user_id);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<Favorite> favoriteList = new ArrayList<Favorite>();
			while (rs.next()) {
				Favorite favorite = new Favorite();
				favorite.setFavorite_id(rs.getInt("favorite_id"));
				favorite.setFavorite_user_id(rs.getInt("favorite_user_id"));
				favorite.setFavorite_item_id(rs.getInt("favorite_item_id"));
				favorite.setItem_create_date(rs.getDate("favorite_item_create_date"));
				favorite.setSeller_id(rs.getInt("item_seller_id"));
				favorite.setItem_name(rs.getString("item_name"));
				favorite.setItem_category_id(rs.getInt("item_category_id"));
				favorite.setItem_detail(rs.getString("item_detail"));
				favorite.setItem_price(rs.getInt("item_price"));
				favorite.setItem_img_name(rs.getString("item_img_name"));
				favorite.setItem_count(rs.getInt("item_count"));
				favorite.setItem_create_date(rs.getDate("item_create_date"));
				favorite.setItem_stock(rs.getInt("item_stock"));

				favoriteList.add(favorite);
			}
			return favoriteList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public Favorite findFavoriteId(int user_id) {
			Connection con = null;
			try {
				//DB接続
				con = DBManager.getConnection();

				String sql = "SELECT " +
						"favorite_table.favorite_id," +
						"favorite_table.favorite_user_id," +
						"favorite_item_table.favorite_id," +
						"favorite_item_table.favorite_item_id," +
						"favorite_item_table.favorite_item_create_date," +
						"item_table.item_id," +
						"item_table.item_seller_id," +
						"item_table.item_name," +
						"item_table.item_category_id," +
						"item_table.item_detail," +
						"item_table.item_price," +
						"item_table.item_img_name," +
						"item_table.item_count," +
						"item_table.item_create_date," +
						"item_table.item_stock " +
						"FROM favorite_table " +
						"JOIN favorite_item_table " +
						"ON favorite_table.favorite_id = favorite_item_table.favorite_id " +
						"JOIN item_table " +
						"ON favorite_item_table.favorite_item_id = item_table.item_id " +
						"WHERE favorite_user_id = ? ;";

				PreparedStatement pstmt = con.prepareStatement(sql);
				pstmt.setInt(1, user_id);
				ResultSet rs = pstmt.executeQuery();

				if (!(rs.next())) {}
					Favorite favorite = new Favorite();
					favorite.setFavorite_id(rs.getInt("favorite_id"));
					favorite.setFavorite_user_id(rs.getInt("favorite_user_id"));
					favorite.setFavorite_item_id(rs.getInt("favorite_item_id"));
					favorite.setItem_create_date(rs.getDate("favorite_item_create_date"));
					favorite.setSeller_id(rs.getInt("item_seller_id"));
					favorite.setItem_name(rs.getString("item_name"));
					favorite.setItem_category_id(rs.getInt("item_category_id"));
					favorite.setItem_detail(rs.getString("item_detail"));
					favorite.setItem_price(rs.getInt("item_price"));
					favorite.setItem_img_name(rs.getString("item_img_name"));
					favorite.setItem_count(rs.getInt("item_count"));
					favorite.setItem_create_date(rs.getDate("item_create_date"));
					favorite.setItem_stock(rs.getInt("item_stock"));

				return favorite;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
	public ArrayList<Favorite> findFavoriteGroup(int user_id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
					"favorite_table.favorite_id," +
					"favorite_table.favorite_user_id," +
					"favorite_group_table.favorite_group_id," +
					"favorite_group_table.favorite_id," +
					"favorite_group_table.favorite_group_name," +
					"favorite_group_table.favorite_group_create_date," +
					"favorite_group_item_table.favorite_group_id," +
					"favorite_group_item_table.favorite_group_item_id," +
					"favorite_group_item_table.favorite_group_item_create_date," +
					"item_table.item_id," +
					"item_table.item_seller_id," +
					"item_table.item_name," +
					"item_table.item_category_id," +
					"item_table.item_detail," +
					"item_table.item_price," +
					"item_table.item_img_name," +
					"item_table.item_count," +
					"item_table.item_create_date," +
					"item_table.item_stock " +
					"FROM favorite_table " +
					"JOIN favorite_group_table " +
					"ON favorite_table.favorite_id = favorite_group_table.favorite_id " +
					"JOIN favorite_group_item_table " +
					"ON favorite_group_table.favorite_group_id = favorite_group_item_table.favorite_group_id " +
					"JOIN item_table " +
					"ON favorite_group_item_table.favorite_item_id = item_table.item_id " +
					"WHERE favorite_user_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, user_id);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<Favorite> favoriteList = new ArrayList<Favorite>();
			while (rs.next()) {
				Favorite favorite = new Favorite();
				favorite.setFavorite_id(rs.getInt("favorite_id"));
				favorite.setFavorite_user_id(rs.getInt("favorite_user_id"));
				favorite.setFavorite_group_id(rs.getInt("favorite_group_id"));
				favorite.setFavorite_group_name(rs.getString("favorite_group_name"));
				favorite.setFavorite_group_create_date(rs.getDate("favorite_group_create_date"));
				favorite.setSeller_id(rs.getInt("item_seller_id"));
				favorite.setItem_name(rs.getString("item_name"));
				favorite.setItem_category_id(rs.getInt("item_category_id"));
				favorite.setItem_detail(rs.getString("item_detail"));
				favorite.setItem_price(rs.getInt("item_price"));
				favorite.setItem_img_name(rs.getString("item_img_name"));
				favorite.setItem_count(rs.getInt("item_count"));
				favorite.setItem_create_date(rs.getDate("item_create_date"));
				favorite.setItem_stock(rs.getInt("item_stock"));

				favoriteList.add(favorite);
			}
			return favoriteList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public Favorite findFavoriteGroupId(String favorite_group_name) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
					"favorite_table.favorite_id," +
					"favorite_table.favorite_user_id," +
					"favorite_group_table.favorite_group_id," +
					"favorite_group_table.favorite_id," +
					"favorite_group_table.favorite_group_name," +
					"favorite_group_table.favorite_group_create_date," +
					"favorite_group_item_table.favorite_group_id," +
					"favorite_group_item_table.favorite_group_item_id," +
					"favorite_group_item_table.favorite_group_item_create_date," +
					"item_table.item_id," +
					"item_table.item_seller_id," +
					"item_table.item_name," +
					"item_table.item_category_id," +
					"item_table.item_detail," +
					"item_table.item_price," +
					"item_table.item_img_name," +
					"item_table.item_count," +
					"item_table.item_create_date," +
					"item_table.item_stock " +
					"FROM favorite_table " +
					"JOIN favorite_group_table " +
					"ON favorite_table.favorite_id = favorite_group_table.favorite_id " +
					"JOIN favorite_group_item_table " +
					"ON favorite_group_table.favorite_group_id = favorite_group_item_table.favorite_group_id " +
					"JOIN item_table " +
					"ON favorite_item_table.favorite_item_id = item_table.item_id " +
					"WHERE favorite_group_name = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, favorite_group_name);
			ResultSet rs = pstmt.executeQuery();

			if (!(rs.next())) {}
				Favorite favorite = new Favorite();
				favorite.setFavorite_id(rs.getInt("favorite_id"));
				favorite.setFavorite_user_id(rs.getInt("favorite_user_id"));
				favorite.setFavorite_group_id(rs.getInt("favorite_group_id"));
				favorite.setFavorite_group_name(rs.getString("favorite_group_name"));
				favorite.setFavorite_group_create_date(rs.getDate("favorite_group_create_date"));
				favorite.setSeller_id(rs.getInt("item_seller_id"));
				favorite.setItem_name(rs.getString("item_name"));
				favorite.setItem_category_id(rs.getInt("item_category_id"));
				favorite.setItem_detail(rs.getString("item_detail"));
				favorite.setItem_price(rs.getInt("item_price"));
				favorite.setItem_img_name(rs.getString("item_img_name"));
				favorite.setItem_count(rs.getInt("item_count"));
				favorite.setItem_create_date(rs.getDate("item_create_date"));
				favorite.setItem_stock(rs.getInt("item_stock"));

			return favorite;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public int FavoriteTableSignUp(int user_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "INSERT INTO favorite_table (favorite_user_id)VALUES(?);";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1,user_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public int FavoriteGroupTableSignUp(int favorite_id,String favorite_group_name) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "INSERT INTO favorite_group_table ("
				+ "favorite_id,"
				+ "favorite_group_name "
				+ ")VALUES(?,?,cast(now() as datetime);";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1,favorite_id);
		pstmt.setString(2,favorite_group_name);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public int FavoriteGroupItemTableSignUp(int favorite_id,int favorite_group_item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "INSERT INTO favorite_group_item_table ("
				+ "favorite_group_id,"
				+ "favorite_group_item_id,"
				+ "favorite_group_item_create_date "
				+ ")VALUES(?,?,cast(now() as datetime);";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1,favorite_id);
		pstmt.setInt(2,favorite_group_item_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
}
