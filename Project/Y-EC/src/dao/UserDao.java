package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.User;

public class UserDao {
	public User findByInfo(String login_id,String password) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user_table WHERE user_login_id = ? and user_password = md5(?)";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			User user = new User();
			user.setUser_id(rs.getInt("user_id"));
			user.setUser_login_id(rs.getString("user_login_id"));
			user.setUser_name(rs.getString("user_name"));
			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public User findByUserData(int user_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
							"user_table.user_id," +
							"user_table.user_name," +
							"area_table.area_id," +
							"area_table.area_name_prefectual " +
							"FROM user_table " +
							"JOIN area_table ON user_table.user_address = area_table.area_id " +
							"WHERE user_table.user_id = ? ";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, user_id);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			int userId = rs.getInt("user_id");
			String user_name = rs.getString("user_name");
			String user_address = rs.getString("area_name_prefectual");
			User user = new User(userId,user_name,user_address);
			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public int userSignUp(String login_id, String password, String name,
				int address, String birth_date) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO `user_table` (" +
					"`user_login_id`," +
					"`user_password`," +
					"`user_name`," +
					"`user_address`," +
					"`user_birth_date`," +
					"`user_create_date`," +
					"`user_update_date`"+
					")VALUES("+
					"?,MD5(?),?,?,cast(? as datetime),cast(now() as datetime),cast(now() as datetime));";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			pstmt.setString(3, name);
			pstmt.setInt(4, address);
			pstmt.setString(5,birth_date);
			int count = pstmt.executeUpdate();
			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
		public int userUpdate(String login_id, String password, String name,
				int address, String birth_date,int user_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "UPDATE `user_table` SET " +
					"`user_login_id = ?`," +
					"`user_password = ?`," +
					"`user_name = ?`," +
					"`user_address = ?`," +
					"`user_birth_date = cast(? as datetime)`," +
					"`user_update_date = cast(now() as datetime) `"+
					"WHERE user_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			pstmt.setString(3, name);
			pstmt.setInt(4, address);
			pstmt.setString(5,birth_date);
			pstmt.setInt(6, user_id);
			int count = pstmt.executeUpdate();
			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
		public int userDelete(int user_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "DELETE FROM `user_table` WHERE user_id = ? ;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, user_id);
			int count = pstmt.executeUpdate();
			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
		public User findByUserDetail(String user_login_id) {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT " +
							"user_table.user_id," +
							"user_table.user_login_id," +
							"user_table.user_name," +
							"user_table.user_birth_date," +
							"user_table.user_create_date," +
							"user_table.user_update_date," +
							"area_table.area_id," +
							"area_table.area_name_prefectual " +
							"FROM user_table " +
							"JOIN area_table ON user_table.user_address = area_table.area_id " +
							"WHERE user_table.user_login_id = ? ;";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, user_login_id);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			int userId = rs.getInt("user_id");
			String userLoginId = rs.getString("user_login_id");
			String userName = rs.getString("user_name");
			Date userBirthDate = rs.getDate("user_birth_date");
			Date userCreateDate = rs.getDate("user_create_date");
			Date userUpdateDate = rs.getDate("user_update_date");
			String userAddress = rs.getString("area_name_prefectual");
			User user = new User(userId,userLoginId,userName,userBirthDate,userCreateDate,userUpdateDate,userAddress);
			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
