package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Item;

public class ItemDao {
	public int itemSignUp(String item_name,int category_id, String item_detail,int item_price,int item_stock,String item_img_name,int seller_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "INSERT INTO item_table(" +
				"item_name," +
				"item_category_id," +
				"item_detail," +
				"item_price," +
				"item_stock," +
				"item_img_name," +
				"item_seller_id,"+
				"item_create_date " +
				")VALUES(?,?,?,?,?,?,?,cast(now() as datetime));";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, item_name);
		pstmt.setInt(2, category_id);
		pstmt.setString(3, item_detail);
		pstmt.setInt(4, item_price);
		pstmt.setInt(5, item_stock);
		pstmt.setString(6, item_img_name);
		pstmt.setInt(7, seller_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public int itemUpdate(String item_name,int category_id, String item_detail,
			String item_price,String item_stock,String item_img_name,String item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "UPDATE `item_table` SET " +
				"`item_name = ?`," +
				"`item_category_id = ?`," +
				"`item_detail` = ?," +
				"`item_price` = ?," +
				"`item_stock` = ?," +
				"`item_img_name = ?`," +
				"`item_update_date` = cast(now() as datetime) " +
				"WHERE item_id = ?;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, item_name);
		pstmt.setInt(2, category_id);
		pstmt.setString(3, item_detail);
		pstmt.setString(4, item_price);
		pstmt.setString(6, item_stock);
		pstmt.setString(7, item_img_name);
		pstmt.setString(8, item_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public int itemUpdateStock(int item_id,int item_stock) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "UPDATE `item_table` SET " +
				"`item_stock` = ? " +
				"WHERE item_id = ?;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, item_stock);
		pstmt.setInt(2, item_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public Item findByItem(int item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT * FROM item_table WHERE item_id = ?;";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, item_id);
		ResultSet rs = pstmt.executeQuery();

		//falseの時にnullを返す
		if (!rs.next()) {
			return null;
		}
		Item item = new Item();
		item.setItem_id(rs.getInt("item_id"));
		item.setItem_name(rs.getString("item_name"));
		item.setSeller_id(rs.getInt("item_seller_id"));
		item.setItem_category_id(rs.getInt("item_category_id"));
		item.setItem_detail(rs.getString("item_detail"));
		item.setItem_price(rs.getInt("item_price"));
		item.setItem_img_name(rs.getString("item_img_name"));
		item.setItem_count(rs.getInt("item_count"));
		item.setItem_create_date(rs.getDate("item_create_date"));
		item.setItem_stock(rs.getInt("item_stock"));
		return item;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public int itemDelete(int item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "DELETE FROM `item_table` WHERE user_id ;";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, item_id);
		int count = pstmt.executeUpdate();
		return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
	public ArrayList<Item> findByItemSellerId(int seller_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT * FROM item_table WHERE item_seller_id = ?;";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, seller_id);
		ResultSet rs = pstmt.executeQuery();

		ArrayList<Item> itemList = new ArrayList<Item>();

		while (rs.next()) {
			Item item = new Item();
			item.setItem_id(rs.getInt("item_id"));
			item.setSeller_id(rs.getInt("item_seller_id"));
			item.setItem_name(rs.getString("item_name"));
			item.setItem_category_id(rs.getInt("item_category_id"));
			item.setItem_detail(rs.getString("item_detail"));
			item.setItem_price(rs.getInt("item_price"));
			item.setItem_img_name(rs.getString("item_img_name"));
			item.setItem_count(rs.getInt("item_count"));
			item.setItem_create_date(rs.getDate("item_create_date"));
			item.setItem_stock(rs.getInt("item_stock"));
			itemList.add(item);
		}

		return itemList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public Item findByItemDataDetail(int item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT " +
				"item_table.item_id," +
				"item_table.item_seller_id," +
				"item_table.item_name," +
				"item_table.item_category_id," +
				"item_table.item_detail," +
				"item_table.item_price," +
				"item_table.item_img_name," +
				"item_table.item_count," +
				"item_table.item_create_date," +
				"item_table.item_stock," +
				"seller_table.seller_name," +
				"item_category_table.category_name," +
				"area_table.area_name_prefectual " +
				"FROM item_table " +
				"JOIN item_category_table  " +
				"ON item_table.item_category_id = item_category_table.category_id " +
				"JOIN seller_table " +
				"ON item_table.item_seller_id = seller_table.seller_id " +
				"JOIN area_table " +
				"ON seller_table.seller_address = area_table.area_id " +
				"WHERE item_table.item_id = ? ;";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, item_id);
		ResultSet rs = pstmt.executeQuery();

		if (!rs.next()) {

		}
			Item item = new Item();
			item.setItem_id(rs.getInt("item_id"));
			item.setSeller_id(rs.getInt("item_seller_id"));
			item.setItem_name(rs.getString("item_name"));
			item.setItem_category_id(rs.getInt("item_category_id"));
			item.setItem_detail(rs.getString("item_detail"));
			item.setItem_price(rs.getInt("item_price"));
			item.setItem_img_name(rs.getString("item_img_name"));
			item.setItem_count(rs.getInt("item_count"));
			item.setItem_create_date(rs.getDate("item_create_date"));
			item.setItem_stock(rs.getInt("item_stock"));
			item.setItem_seller_name(rs.getString("seller_name"));
			item.setItem_category_name(rs.getString("category_name"));
			item.setItem_seller_address_name(rs.getString("area_name_prefectual"));
		return item;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public ArrayList<Item> indexItem(int category_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT " +
				"item_table.item_id," +
				"item_table.item_seller_id," +
				"item_table.item_name," +
				"item_table.item_category_id," +
				"item_table.item_detail," +
				"item_table.item_price," +
				"item_table.item_count," +
				"item_table.item_create_date," +
				"item_table.item_stock," +
				"item_table.item_img_name," +
				"item_category_table.category_id," +
				"item_category_table.category_name " +
				"FROM item_table " +
				"JOIN item_category_table " +
				"ON item_table.item_category_id = item_category_table.category_id " +
				"WHERE item_category_id = ? " +
				"ORDER BY item_count DESC limit 3 ;";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, category_id);
		ResultSet rs = pstmt.executeQuery();
		ArrayList<Item> itemList = new ArrayList<Item>();

		while (rs.next()) {
			Item item = new Item();
			item.setItem_id(rs.getInt("item_id"));
			item.setSeller_id(rs.getInt("item_seller_id"));
			item.setItem_name(rs.getString("item_name"));
			item.setItem_category_id(rs.getInt("item_category_id"));
			item.setItem_detail(rs.getString("item_detail"));
			item.setItem_price(rs.getInt("item_price"));
			item.setItem_img_name(rs.getString("item_img_name"));
			item.setItem_count(rs.getInt("item_count"));
			item.setItem_create_date(rs.getDate("item_create_date"));
			item.setItem_stock(rs.getInt("item_stock"));
			item.setItem_category_name(rs.getString("category_name"));
			itemList.add(item);
		}

		return itemList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public ArrayList<Item> itemSerch(int category_id,String item_name) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT * FROM item_table WHERE item_category_id = ? and item_name LIKE '%"+ item_name +"%' limit 3 ;";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, category_id);
		ResultSet rs = pstmt.executeQuery();

		ArrayList<Item> itemList = new ArrayList<Item>();

		while (rs.next()) {
			Item item = new Item();
			item.setItem_id(rs.getInt("item_id"));
			item.setSeller_id(rs.getInt("item_seller_id"));
			item.setItem_name(rs.getString("item_name"));
			item.setItem_category_id(rs.getInt("item_category_id"));
			item.setItem_detail(rs.getString("item_detail"));
			item.setItem_price(rs.getInt("item_price"));
			item.setItem_img_name(rs.getString("item_img_name"));
			item.setItem_count(rs.getInt("item_count"));
			item.setItem_create_date(rs.getDate("item_create_date"));
			item.setItem_stock(rs.getInt("item_stock"));
			itemList.add(item);
		}

		return itemList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public void itemCountAdd(int item_id) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT * FROM item_table WHERE item_id = ? ORDER BY item_id DESC limit 1;";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, item_id);
		ResultSet rs = pstmt.executeQuery();

		if (!rs.next()) {}
			Item item = new Item();
			item.setItem_id(rs.getInt("item_id"));
			item.setItem_count(rs.getInt("item_count"));
			item.setItem_stock(rs.getInt("item_stock"));
		if(item.getItem_id() != 0) {
			int count = item.getItem_count() + 1;

			String sqlUpdate = "UPDATE item_table SET item_count = ?  WHERE item_id = ? ";

			PreparedStatement pstmtUpdate = con.prepareStatement(sqlUpdate);
			pstmtUpdate.setInt(1, count);
			pstmtUpdate.setInt(2, item.getItem_id());
			pstmtUpdate.executeUpdate();
			}
		if(item.getItem_id() == 0) {
			return;
		}

		} catch (SQLException e) {
			e.printStackTrace();
			return;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}
	public static ArrayList<Item> getItemsByItemName(String itemSerch,int categoryId , int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (itemSerch.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM item_table WHERE item_category_id = ? ORDER BY item_id ASC LIMIT ?,? ");
				st.setInt(1, categoryId);
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			} else {
				// 商品名検索
				st = con.prepareStatement("SELECT * FROM item_table WHERE item_name LIKE ? AND item_category_id = ? ORDER BY item_id ASC LIMIT ?,? ");
				st.setString(1,"%"+itemSerch+"%");
				st.setInt(2, categoryId);
				st.setInt(3, startiItemNum);
				st.setInt(4, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<Item> itemList = new ArrayList<Item>();

			while (rs.next()) {
				Item item = new Item();
				item.setItem_id(rs.getInt("item_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setItem_detail(rs.getString("item_detail"));
				item.setItem_price(rs.getInt("item_price"));
				item.setItem_img_name(rs.getString("item_img_name"));
				item.setItem_category_id(rs.getInt("item_category_id"));
				itemList.add(item);
			}
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	public static double getItemCount(String itemName,int categoryId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt FROM item_table WHERE item_name like ? AND item_category_id = ?");
			st.setString(1, "%" + itemName + "%");
			st.setInt(2, categoryId);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	public ArrayList<Item> buyItemData(int sellerId) {
		Connection con = null;
	try {
		//DB接続
		con = DBManager.getConnection();

		String sql = "SELECT " +
				"buy_detail_table.buy_detail_buy_id," +
				"buy_detail_table.buy_detail_user_id," +
				"buy_detail_table.buy_detail_item_id," +
				"item_table.item_id," +
				"item_table.item_seller_id," +
				"item_table.item_name," +
				"item_table.item_category_id," +
				"item_table.item_detail," +
				"item_table.item_price," +
				"item_table.item_count," +
				"item_table.item_create_date," +
				"item_table.item_stock," +
				"buy_table.buy_id," +
				"buy_table.buy_user_id," +
				"buy_table.delivary_method," +
				"buy_table.delivary_area_id," +
				"buy_table.buy_total_price," +
				"buy_table.buy_delivary_id," +
				"buy_table.buy_create_date " +
				"FROM buy_detail_table " +
				"JOIN item_table " +
				"ON buy_detail_table.buy_detail_item_id = item_table.item_id " +
				"JOIN buy_table " +
				"ON buy_detail_table.buy_detail_buy_id = buy_table.buy_id " +
				"WHERE item_table.item_seller_id = ? ;";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1,sellerId);
		ResultSet rs = pstmt.executeQuery();

		ArrayList<Item> itemList = new ArrayList<Item>();

		while (rs.next()) {
			Item item = new Item();
			item.setItem_id(rs.getInt("item_id"));
			item.setSeller_id(rs.getInt("item_seller_id"));
			item.setItem_name(rs.getString("item_name"));
			item.setItem_category_id(rs.getInt("item_category_id"));
			item.setItem_detail(rs.getString("item_detail"));
			item.setItem_price(rs.getInt("item_price"));
			item.setItem_count(rs.getInt("item_count"));
			item.setItem_create_date(rs.getDate("item_create_date"));
			item.setItem_stock(rs.getInt("item_stock"));
			itemList.add(item);
		}

		return itemList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}
