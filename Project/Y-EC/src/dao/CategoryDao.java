package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Category;

public class CategoryDao {
		public ArrayList<Category> findAll() {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM item_category_table;";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<Category> category_list = new ArrayList<Category>();

			while (rs.next()) {
				Category category = new Category();
				category.setCategory_id(rs.getInt("category_id"));
				category.setCategory_name(rs.getString("category_name"));
				category.setCategory_create_date(rs.getDate("category_create_date"));
				category.setCategory_img_name(rs.getString("category_img_name"));
				category_list.add(category);
			}

			return category_list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public int findcount() {
			Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM item_category_table;";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			int count = 0;

			while (rs.next()) {
				count ++;
			}

			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
		public Category findByCategoryId(String category_name) {
			Connection con = null;
			try {
				//DB接続
				con = DBManager.getConnection();

				String sql = "SELECT * FROM item_category_table WHERE category_name = ? ";

				PreparedStatement pstmt = con.prepareStatement(sql);
				pstmt.setString(1, category_name);
				ResultSet rs = pstmt.executeQuery();

				//falseの時にnullを返す
				if (!rs.next()) {
					return null;
				}
				int categoryId = rs.getInt("category_id");
				Category category = new Category(categoryId);
				return category;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
}
